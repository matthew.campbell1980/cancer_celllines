import  Header  from 'antd/lib/layout';
import {Menu}  from 'antd';
import SubMenu from 'antd/lib/menu/SubMenu';
import CodepenOutlined from '@ant-design/icons';

//<Menu.Item key="3"><a href="/cellline">Cell Lines</a></Menu.Item>

const HeaderPage = () => (
  <Header>
    <div className="logo" />
      <Menu
        theme="dark"
        mode="horizontal"
        //defaultSelectedKeys={['2']}
        style={{ lineHeight: '48px' }}
      >
        <Menu.Item key="1"><a href="http://www.unicarbkb.org">UniCarbKB</a></Menu.Item>
        <Menu.Item key="2"><a href="http://www.glygen.org">GlyGen</a></Menu.Item>
        <Menu.Item key="3"><a href="/glycoproteins">Glycoproteins</a></Menu.Item>
        
        <Menu.Item key="3"><a href="/sparql">SPARQL Endpoint</a></Menu.Item>

        <SubMenu title={<><CodepenOutlined /><span>GlycoStore</span></>}>
            <Menu.Item key="setting:1"><a href="https://glycostore.org/melanoma">Melanoma RT</a></Menu.Item>
        </SubMenu>


        <SubMenu title={<><CodepenOutlined /><span>Packages/Code</span></>}>
            <Menu.Item key="setting:1"><a href="https://gitlab.com/matthew.campbell1980/rmelanomaglycomics">MelanomaGlycomicsR</a></Menu.Item>
            <Menu.Item key="setting:3"><a href="https://gitlab.com/matthew.campbell1980/cancer_celllines.git">GitLab</a></Menu.Item>
        </SubMenu>

        
      </Menu>
    </Header>
  );
  
  export default HeaderPage;
