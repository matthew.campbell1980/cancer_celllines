import React from 'react';
import { Typography } from 'antd';

const { Text } = Typography;

const Instrumentation = props => (

  <div><Text strong>Instrumentation:</Text><Text>{props.instrument}</Text>


  </div>

);

export default Instrumentation;

