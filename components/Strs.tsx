import { Typography, Alert } from 'antd';
import { Badge } from 'antd';
const { Text } = Typography;

const Strs = props => (

    <div>
        <Text>Structural Annotations </Text><Badge count={props.strCount} style={{ backgroundColor: '#108ee9' }} showZero  />
    </div>
);

export default Strs;

