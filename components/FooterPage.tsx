import Layout from 'antd/lib/layout';
import { Helmet } from 'react-helmet';
const { Footer } = Layout;

let s = '<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Creative Commons Licence" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png" /></a><br /><span xmlns:dct="http://purl.org/dc/terms/" property="dct:title">UniCarbKB</span> by <a xmlns:cc="http://creativecommons.org/ns#" href="www.unicarbkb.org" property="cc:attributionName" rel="cc:attributionURL">UniCarbKB</a> is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License</a>.'

const FooterPage = () => (
  <Footer style={{ textAlign: 'center' }}>
    This project is supported by the Australian Research Data Commons (ARDC) and by NIH Glycoscience Common Fund. The ARDC is enabled by NCRIS. 
    <br />
    UniCarbKB-GlyGen ©2020 
    <div
  className="license"
  dangerouslySetInnerHTML={{
    __html: `
    <a rel="license" href="http://creativecommons.org/licenses/by-nc/4.0/"><img alt="Creative Commons Licence" style="border-width:0" src="https://i.creativecommons.org/l/by-nc/4.0/80x15.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc/4.0/">Creative Commons Attribution-NonCommercial 4.0 International License</a>. `
  }}
/>
   
    </Footer>    
);
  
  export default FooterPage;

