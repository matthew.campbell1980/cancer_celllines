import { Typography, Alert } from 'antd';
const { Text } = Typography;

const GlygenNote = () => (
        <div>
            <Alert message="Work In Progress" type="info" description="In partnership with the GlyGen data model team UniCarbKB is migrating to a new platform. This includes addition of newly curated data collections
        and improvements to legacy data." showIcon/>
        </div>
);

export default GlygenNote;