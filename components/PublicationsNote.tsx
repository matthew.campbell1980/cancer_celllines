import { Alert } from "antd";



const PublicationsNote = props => (
    <div>
        <Alert message="Work In Progress" type="info" description="In partnership with the GlyGen data model team UniCarbKB is migrating to a new platform. This includes addition of newly curated data collections
and improvements to legacy data. You are viewing curated site-specific publications." showIcon/>
    </div>
);

export default PublicationsNote;