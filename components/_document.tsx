// _document is only rendered on the server side and not on the client side
// Event handlers like onClick can't be added to this file

// ./pages/_document.js
import Document, { Html, Head, Main, NextScript } from 'next/document'

class MyDocument extends Document {
  static async getInitialProps(ctx) {
    const initialProps = await Document.getInitialProps(ctx)
    return { ...initialProps }
  }

  render() {
    return (
      <Html>
        <Head />
        <body>
          <Main />
          <NextScript />
        </body>
        <script src="http://ebi-uniprot.github.io/CDN/protvista/protvista.js"></script>
<link href="http://ebi-uniprot.github.io/CDN/protvista/css/main.css" rel="stylesheet"/>
<script src="https://cdn.jsdelivr.net/npm/protvista-utils@latest/dist/protvista-utils.js" defer ></script>
    <script src="https://cdn.jsdelivr.net/npm/data-loader@latest/dist/data-loader.js" defer></script>
    <script src="https://cdn.jsdelivr.net/npm/protvista-feature-adapter@latest/dist/protvista-feature-adapter.js" defer></script>
    <script src="https://cdn.jsdelivr.net/npm/protvista-zoomable@latest/dist/protvista-zoomable.js" defer ></script>
    <script src="https://cdn.jsdelivr.net/npm/protvista-track@latest/dist/protvista-track.js" defer></script>

      </Html>
    )
  }
}

export default MyDocument