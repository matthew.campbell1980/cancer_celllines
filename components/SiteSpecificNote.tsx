import { Typography, Alert } from 'antd';
const { Text } = Typography;

const SiteSpecificNote = () => (
        <div>
            <Text type="secondary">Following section show glycan structure described at a site-specific level</Text>
        <br /><br/>
        </div>
);
