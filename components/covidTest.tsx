import { Chart, Tooltip, Axis, StackBar, Coord, Legend } from 'viser-react';
import * as React from 'react';
import { Typography } from 'antd';


const { Text, Title  } = Typography;

const DataSet = require('@antv/data-set');

const sourceData = [

    {'position': '17', 'M5': 4, 'FH': 1, 'FA1': 9, 'FA2/FA1B': 33, 'FA3/FA2B': 49, 'FA4/FA3B': 3 },
    {'position': '61', 'M5': 76, 'H':  6, 'FH':  1, 'A1':  5, 'FA1':  1, 'A2/A1B':  1, 'FA2/FA1B':  4, 'A3/A2B':  3, 'A4/A3B':  1 }, 
    {"position": '74', "A1":  2,"A2/A1B":  14 ,"FA2/FA1B":  4,"A3/A2B":  28,"FA3/FA2B":  33,"A4/A3B":  8,"FA4/FA3B":  9 },
{"position": '122', "M5":  46,"H":   6,"A1":  5,"FA1":  11,"A2/A1B":  3,"FA2/FA1B":  23,"A3/A2B":  6 },
{"position": '149', "M5":  13,"FA1":  8,"A2/A1B":  1,"FA2/FA1B":  60,"FA3/FA2B":  18 },
{"position": '165', "M5":  34,"FA2/FA1B":  59,"FA3/FA2B":  7 },
{"position": '234', "M9":  34,"M8":  42,"M7":  15,"M6":  3, "M5":  3,"FA1":  1,"FA2/FA1B":  1 },
{"position": '282', "M5":  3,"A2/A1B":  3,"FA2/FA1B":  7,"A3/A2B":  38,"FA3/FA2B":  48,"A4/A3B":  1 },
{"position": '331', "M5":  1,"FA1":  3,"FA2/FA1B":  55,"FA3/FA2B":  37,"FA4/FA3B":  3 },
{"position": '343', "FH":   5,"FA1":  18,"FA2/FA1B":  72,"FA3/FA2B":  6 },
{"position": '603', "M6":  4,"M5":  49,"H":   8,"FH":   1,"A1":  3,"FA1":  5,"FA2/FA1B":  29 },
{"position": '616', "M5":  6,"H":   3,"FH":   1,"A1":  11,"FA1":  1,"A2/A1B":  19,"FA2/FA1B":  44,"A3/A2B":  6,"FA3/FA2B":  4,"FA4/FA3B":  4 },
{"position": '657', "M7":  4,"M6":  32,"H":   64 },
{"position": '709', "M6":  33,"M5":  65,"FA3/FA2B":  2 },
{"position": '717', "M7":  15,"M6":  24,"M5":  35,"H":   21,"A1":  3,"FA1":  2 },
{"position": '801', "M7":  10,"M6":  31,"M5":  48,"A2/A1B":  1,"FA2/FA1B":  9 },
{"position": '1074', "M6":  1,"M5":  54,"H":   8,"FH":   1,"A1":  5,"FA1":  5,"A2/A1B":  2,"FA2/FA1B":  12,"FA3/FA2B":  10 },
{"position": '1098', "M6":  1,"M5":  6,"H":   13,"FH":   2,"A1":  8,"FA1":  12,"A2/A1B":  18,"FA2/FA1B":  15,"A3/A2B":  8,"FA3/FA2B":  9,"A4/A3B":  4,"FA4/FA3B":  4 },
{"position": '1134', "FA1":  25,"FA2/FA1B":  41,"FA3/FA2B":  22,"FA4/FA3B":  12 },
{"position": '1158', "A1":  4,"A2/A1B":  96 },
{"position": '1173', "FA4/FA3B":  100 },
{"position": '1194', "FA4/FA3B":  100 },





  /*{ 'State': '61', '小于5岁': 10, '5至13岁': 5, '14至17岁': 85 },
  { 'State': '61', '小于5岁': 70, '5至13岁': 15, '14至17岁': 15 },
  { 'State': '61', '小于5岁': 50, '5至13岁': 20, '14至17岁': 30 },
  { 'State': '71', 'man': 80, '5至13岁': 10, '14至17岁': 10 },
  { 'State': '71', 'man': 50, '5至13岁': 40, '14至17岁': 10 }*/

  /*{ country: 'N17', year: 'A1', value: 10 },
  { country: 'N17', year: 'A2', value: 20 },
  { country: 'N17', year: 'FA1', value: 30 },
  { country: 'N17', year: 'M5', value: 30 },
  { country: 'N17', year: 'M7', value: 10 },
  
  { country: 'N87', year: 'A1', value: 50 },
  { country: 'N87', year: 'FA1', value: 50 },*/
];

const dv = new DataSet.View().source(sourceData);
dv.transform({
  type: 'fold',
  fields: [ 'M9', 'M8', 'M7', 'M6','M5', 'FH', 'H', 'A1', 'FA1', 'A2/A1B', 'FA2/FA1B', 'A3/A2B', 'FA3/FA2B', 'A4/A3B', 'FA4/FA3B'],//['小于5岁', '5至13岁', '14至17岁'],
  key: '年龄段',
  value: '人口数量',
  retains: ['position'],
});
const data = dv.rows;


/*const dv = new DataSet.View().source(sourceData);
dv.transform({
    type: 'fold',
    fields: ['M5', 'FH', 'FA1', 'FA2/FA1B', 'FA3/FA2B', 'FA4/FA3B'], //['小于5岁', '5至13岁', '14至17岁' ], ////['M5', 'FH', 'FA1', 'FA2/FA1B', 'FA3/FA2B', 'FA4/FA3B'],
    key: '年龄段',
    value: '人口数量',
    retains: ['State'],
});*/

const label = {
  textStyle: {
    fill: '#aaaaaa'
  }
}

const tickLine = {
  alignWithLabel: false,
  length: 0
}

const title = {
  offset: 30,
  textStyle: {
    fontSize: 14,
    fontWeight: 300
  }
}

const line = {
  lineWidth: 0
} 


export default class App extends React.Component {
  render() {
    return (

      <Chart forceFit height={400} data={data} padding={[20, 20, 50, 140]} >
        <Coord type="rect" direction="LB" />
        <Tooltip />
        <Axis dataKey="position" label={label} tickLine={tickLine} line={line}/>
        <Legend position='top-center'/>
        <Axis dataKey="position" label={{ offset: 12 }} />
        <StackBar position="position*人口数量" color="年龄段" />
      </Chart>
    );
  }
}