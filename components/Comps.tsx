import { Typography, Alert } from 'antd';
import { Badge } from 'antd';
const { Text } = Typography;

const Comps = props => (

    <div>
        <Text>Compositional Annotations </Text><Badge  count={props.compsCount} style={{ backgroundColor: '#87d068' }}/>
    <br />
    </div>
);

export default Comps;

