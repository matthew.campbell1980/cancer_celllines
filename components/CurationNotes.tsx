import React from 'react';
import { Typography } from 'antd';

const { Text } = Typography;

const CurationNotes = props => (
    //console.log("sjsjs" + props);
    <div>
        <Text strong>Expression: </Text><Text>{props.expression}</Text>
        <Text strong>Additional Notes: </Text><Text>{props.additionalNotes}</Text>
    </div>

);

export default CurationNotes;