import React from 'react';
import { Typography } from 'antd';

const { Text } = Typography;

const Glycopeptidedigest = props => (
  <div><Text strong>Enzymes:</Text><Text>{props.gpenzymes.toString().charAt(0).toUpperCase() + props.gpenzymes.toString().slice(1)}</Text>
  </div>
);

export default Glycopeptidedigest