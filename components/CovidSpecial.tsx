import { Typography, Alert, Button } from 'antd';
import {BookOutlined} from '@ant-design/icons';
const { Title, Text } = Typography;

const CovidSpecial = () => (
        <div>
            <Alert message="SARS-Cov-2" type="warning" description="The data presented is sourced from 2 peer-reviewed publications and 1 preprint" showIcon />
            <Title level={3}>Publications</Title>
            <p><Text strong>Watanabe et al., Site-specific glycan analysis of the SARS-CoV-2 spike, Science, 2020 doi: <a href="https://doi.org/10.1126/science.abb9983">https://doi.org/10.1101/2020.03.26.010322</a> </Text>  <a href="/glycoprotein/P0DTC2?pubmed=32366695">Show Data</a><br />
            <Text strong>Zhang, Y et al., Site-specific N-glycosylation Characterization of Recombinant SARS-CoV-2 Spike Proteins, doi: <a href="https://doi.org/10.1101/2020.03.28.013276">https://doi.org/10.1101/2020.03.28.013276</a>  <a href="/glycoprotein/P0DTC2?pubmed=0">Show Data</a></Text><br />
            <Text strong>Shajahan, A et al., Deducing the N- and O- glycosylation profile of the spike protein of novel coronavirus SARS-CoV-2 Glycobiology, 2020 doi: <a href="https://doi.org/10.1093/glycob/cwaa042">https://doi.org/10.1093/glycob/cwaa042</a><a href="/glycoprotein/P0DTC2?pubmed=32363391">Show Data</a></Text></p>
        
        </div>
);

export default CovidSpecial;