import React from 'react';
import { Typography } from 'antd';
const { Text } = Typography;

const Fragmentation = props => (
  <div><Text strong>MS Fragmentation:</Text><Text>{props.fragmentation}</Text>
  </div>
);

export default Fragmentation;