import { PageHeader, Typography, Layout } from 'antd';
import Link from 'next/link';
import { useRouter } from 'next/router';

const { Paragraph } = Typography;

const routes = [
  {
    path: 'index',
    breadcrumbName: 'First-level Menu',
  },
  {
    path: 'first',
    breadcrumbName: 'Second-level Menu',
  },
  {
    path: 'second',
    breadcrumbName: 'Third-level Menu',
  },
];

const content = (
  <div className="content">
    <Paragraph>
      Ant Design interprets the color system into two levels: a system-level color system and a
      product-level color system.
    </Paragraph>
    <Paragraph>
      Ant Design&#x27;s design team preferred to design with the HSB color model, which makes it
      easier for designers to have a clear psychological expectation of color when adjusting colors,
      as well as facilitate communication in teams.
    </Paragraph>
    <p className="contentLink">
      <a>
        <img
          src="https://gw.alipayobjects.com/zos/rmsportal/MjEImQtenlyueSmVEfUD.svg"
          alt="start"
        />
        Quick Start
      </a>
      <a>
        <img src="https://gw.alipayobjects.com/zos/rmsportal/NbuDUAuBlIApFuDvWiND.svg" alt="info" />
        Product Info
      </a>
      <a>
        <img src="https://gw.alipayobjects.com/zos/rmsportal/ohOEPSYdDTNnyMbGuyLb.svg" alt="doc" />
        Product Doc
      </a>
    </p>
  </div>
);

const extraContent = (
  <img
    src="https://gw.alipayobjects.com/mdn/mpaas_user/afts/img/A*KsfVQbuLRlYAAAAAAAAAAABjAQAAAQ/original"
    alt="content"
  />
);

function Review({ blogText , test }) {
  return (
  <PageHeader title="Title" breadcrumb={{ routes }}>
    <div className="wrap">
      <div className="content">{content}</div>
      <div className="extraContent">{extraContent}</div>
      <div><p>sjsjsjsj {blogText} with xxx { 
          (test =>  ( test.product_id
          ) 
          )
        }

        </p></div>
      
    </div>
  </PageHeader>
  )
}

Review.getInitialProps = async function({ query }) {
  // `blogId` will be `'how-to-use-dynamic-routes'` when rendering
  // `/blog/how-to-use-dynamic-routes`
  const blogId = query.id
  const res = await fetch('https://swift.rc.nectar.org.au/v1/AUTH_1507c81fb388480f88312a2d2f9189cd/nextjstest/example.json');
    const json = await res.json();
  console.log("id " + blogId + " res " + res.status + json.shopping_list.cart_items.map( t => t.product_id) );


  return { blogText: blogId , test: json.shopping_list.cart_items.map }
}

export default Review