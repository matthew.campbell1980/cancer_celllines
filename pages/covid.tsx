import fetch from 'isomorphic-unfetch';
import dynamic from 'next/dynamic'
import { Layout, Breadcrumb, Descriptions, Tag, Tooltip, List, Typography, Tabs, Table } from 'antd'


import Content from 'antd/lib/layout';
import HeaderPage from '../components/HeaderPage';
import FooterPage from '../components/FooterPage';


const { TabPane } = Tabs;

const { Text, Title  } = Typography;

const columns = [
    {
        title: 'Image',
        dataIndex: 'glytoucan',
        key: '',
        width: '40%',
        fixed: true,
        render: (text) => <img src={"https://storage.googleapis.com/data.unicarbkb.org/covid/imgs/lg-snfg-" + text + ".png" }  />
      },
    {
      title: 'Uoxf',
      dataIndex: 'uoxf',
      key: 'uoxf',
      width: '20%',
    },
    {
      title: 'GlyToucan',
      dataIndex: 'glytoucan',
      key: 'glytoucan',
      width: '10%'
    },
    {
      title: 'Rel. Area (%)',
      dataIndex: 'abundance',
      key: 'abundance',
      width: '10%'
    }
  ];

/*<div>
      <DynamicComponentWithCustomLoading />
      <p>HOME PAGE is here!</p>
    </div>*/


const DynamicComponentWithCustomLoading = dynamic(
  () => import('../components/covidTest'),
  { loading: () => <p>LOADING... </p> }
)

function callback(key) {
    console.log(key);
}

function sarscov2( { data } ) {

    const site17 = [] 
    const site61 = [] 
    const site74 = [] 
    const site122 = [] 
    const site149 = [] 
    const site165 = [] 
    const site234 = [] 
    const site282 = [] 
    const site331 = [] 

    const site343 = [] 
    const site603 = [] 
    const site616 = [] 
    const site657 = [] 
    const site709 = [] 
    const site717 = [] 
    const site801 = [] 
    const site1074 = [] 
    const site1098 = [] 

    const site1134 = [] 
    const site1158 = [] 
    const site1173 = [] 
    const site1194 = [] 

    data.coviddataset.structures.filter(t => t.position == 17).map(x => site17.push( { uoxf: x.uoxf, glytoucan: x.glytoucan, abundance: x.abundance } )  )
    site17.sort(function (a, b) {
        return a.abundance - b.abundance;
    });

    data.coviddataset.structures.filter(t => t.position == 61).map(x => site61.push( { uoxf: x.uoxf, glytoucan: x.glytoucan, abundance: x.abundance } )  )
    site61.sort(function (a, b) {
        return a.abundance - b.abundance;
    });

    data.coviddataset.structures.filter(t => t.position == 74).map(x => site74.push( { uoxf: x.uoxf, glytoucan: x.glytoucan, abundance: x.abundance } )  )
    site74.sort(function (a, b) {
        return a.abundance - b.abundance;
    });

    data.coviddataset.structures.filter(t => t.position == 122).map(x => site122.push( { uoxf: x.uoxf, glytoucan: x.glytoucan, abundance: x.abundance } )  )
    site122.sort(function (a, b) {
        return a.abundance - b.abundance;
    });

    data.coviddataset.structures.filter(t => t.position == 149).map(x => site149.push( { uoxf: x.uoxf, glytoucan: x.glytoucan, abundance: x.abundance } )  )
    site149.sort(function (a, b) {
        return a.abundance - b.abundance;
    });

    data.coviddataset.structures.filter(t => t.position == 165).map(x => site165.push( { uoxf: x.uoxf, glytoucan: x.glytoucan, abundance: x.abundance } )  )
    site165.sort(function (a, b) {
        return a.abundance - b.abundance;
    });

    data.coviddataset.structures.filter(t => t.position == 234).map(x => site234.push( { uoxf: x.uoxf, glytoucan: x.glytoucan, abundance: x.abundance } )  )
    site234.sort(function (a, b) {
        return a.abundance - b.abundance;
    });

    data.coviddataset.structures.filter(t => t.position == 282).map(x => site282.push( { uoxf: x.uoxf, glytoucan: x.glytoucan, abundance: x.abundance } )  )
    site282.sort(function (a, b) {
        return a.abundance - b.abundance;
    });

    data.coviddataset.structures.filter(t => t.position == 331).map(x => site331.push( { uoxf: x.uoxf, glytoucan: x.glytoucan, abundance: x.abundance } )  )
    site331.sort(function (a, b) {
        return a.abundance - b.abundance;
    });

    data.coviddataset.structures.filter(t => t.position == 343).map(x => site343.push( { uoxf: x.uoxf, glytoucan: x.glytoucan, abundance: x.abundance } )  )
    site343.sort(function (a, b) {
        return a.abundance - b.abundance;
    });

    data.coviddataset.structures.filter(t => t.position == 603).map(x => site603.push( { uoxf: x.uoxf, glytoucan: x.glytoucan, abundance: x.abundance } )  )
    site603.sort(function (a, b) {
        return a.abundance - b.abundance;
    });

    data.coviddataset.structures.filter(t => t.position == 616).map(x => site616.push( { uoxf: x.uoxf, glytoucan: x.glytoucan, abundance: x.abundance } )  )
    site616.sort(function (a, b) {
        return a.abundance - b.abundance;
    });


    data.coviddataset.structures.filter(t => t.position == 657).map(x => site657.push( { uoxf: x.uoxf, glytoucan: x.glytoucan, abundance: x.abundance } )  )
    site657.sort(function (a, b) {
        return a.abundance - b.abundance;
    });

    data.coviddataset.structures.filter(t => t.position == 709).map(x => site709.push( { uoxf: x.uoxf, glytoucan: x.glytoucan, abundance: x.abundance } )  )
    site709.sort(function (a, b) {
        return a.abundance - b.abundance;
    });

    data.coviddataset.structures.filter(t => t.position == 717).map(x => site717.push( { uoxf: x.uoxf, glytoucan: x.glytoucan, abundance: x.abundance } )  )
    site717.sort(function (a, b) {
        return a.abundance - b.abundance;
    });


    data.coviddataset.structures.filter(t => t.position == 801).map(x => site801.push( { uoxf: x.uoxf, glytoucan: x.glytoucan, abundance: x.abundance } )  )
    site801.sort(function (a, b) {
        return a.abundance - b.abundance;
    });

    data.coviddataset.structures.filter(t => t.position == 1074).map(x => site1074.push( { uoxf: x.uoxf, glytoucan: x.glytoucan, abundance: x.abundance } )  )
    site1074.sort(function (a, b) {
        return a.abundance - b.abundance;
    });

    data.coviddataset.structures.filter(t => t.position == 1098).map(x => site1098.push( { uoxf: x.uoxf, glytoucan: x.glytoucan, abundance: x.abundance } )  )
    site1098.sort(function (a, b) {
        return a.abundance - b.abundance;
    });


    data.coviddataset.structures.filter(t => t.position == 1134).map(x => site1134.push( { uoxf: x.uoxf, glytoucan: x.glytoucan, abundance: x.abundance } )  )
    site1134.sort(function (a, b) {
        return a.abundance - b.abundance;
    });

    data.coviddataset.structures.filter(t => t.position == 1158).map(x => site1158.push( { uoxf: x.uoxf, glytoucan: x.glytoucan, abundance: x.abundance } )  )
    site1158.sort(function (a, b) {
        return a.abundance - b.abundance;
    });

    data.coviddataset.structures.filter(t => t.position == 1173).map(x => site1173.push( { uoxf: x.uoxf, glytoucan: x.glytoucan, abundance: x.abundance } )  )
    site1173.sort(function (a, b) {
        return a.abundance - b.abundance;
    });

    data.coviddataset.structures.filter(t => t.position == 1194).map(x => site1194.push( { uoxf: x.uoxf, glytoucan: x.glytoucan, abundance: x.abundance } )  )
    site1194.sort(function (a, b) {
        return a.abundance - b.abundance;
    });

  return (
    <Layout className="layout">
    <HeaderPage />
    <Content style={{ padding: '0 50px' }}>
      <Breadcrumb style={{ margin: '16px 0' }}>
        <Breadcrumb.Item>UniCarbKB-GlyGen</Breadcrumb.Item>
        <Breadcrumb.Item>COVID</Breadcrumb.Item>
      </Breadcrumb>
      <div style={{ background: '#fff', padding: 20, minHeight: 280 }}> 
      <Title level={2}>SARS-CoV-2 S Protein</Title>
      <Descriptions title="" layout="horizontal" column={1}>
        <Descriptions.Item label="Project">Site-specific analysis of the SARS-CoV-2 glycan shield</Descriptions.Item>
        <Descriptions.Item label="Publication">doi: <a href="https://doi.org/10.1101/2020.03.26.010322">https://doi.org/10.1101/2020.03.26.010322</a></Descriptions.Item>
        <Descriptions.Item label="Subjects">
          <Tag color="#d20962"><span>Glycomics</span></Tag>
          <Tag color="#d20962"><span>Glycobiology</span></Tag> 
          <Tag color="#d20962"><span>Site-Specific Analysis</span></Tag> 
        </Descriptions.Item>

        <Descriptions.Item label="Expression">
            <Tag color="#d20962"><span>FreeStyle 293-F</span></Tag> 
            <Tag color="#d20962"><span>SARS-CoV-2 S protein</span></Tag> 
            <br/>
            
        </Descriptions.Item>
        <Text>From publication: Gene encoding residues 1−1208 of SARS-CoV-2 S
(GenBank: MN908947) with proline substitutions at residues 986 and 987, a “GSAS”
substitution at the furin cleavage site (residues 682–685), a C-terminal T4 fibritin trimerization
motif, an HRV3C protease cleavage site, a TwinStrepTag and an 8XHisTag was synthesized
and cloned into the mammalian expression vector pαH. This expression vector was used to
transiently transfect FreeStyle293F cells (Thermo Fisher) using polyethylenimine. Protein was
purified from filtered cell supernatants using StrepTactin resin (IBA) before being subjected to
additional purification by size-exclusion chromatography</Text>


    
        <Descriptions.Item label="Structure Model">
          <Tag color="#00a78e"><span>SWISS_MODEL: YP_009724390.1</span></Tag>
          <Tag color="#00a78e"><span>PDB: 6VSB</span></Tag> 
        </Descriptions.Item>
    </Descriptions>

    <div>
    <Title level={4}>Refine chart by selecting structures listed</Title>
      <Text>'H' = Hybrid and 'FH' = Fucosylated Hybrid</Text>
    </div>

    <Tabs  onChange={callback}>
        
        <TabPane tab="N17" key="1">
            <Table dataSource={site17} columns={columns} pagination={{ pageSize: 100 }} scroll={{ y: 480 }} />
        </TabPane>
        <TabPane tab="N61" key="2">
            <Table dataSource={site61} columns={columns} pagination={{ pageSize: 100 }} scroll={{ y: 480 }} />
        </TabPane>
        <TabPane tab="N74" key="3">
            <Table dataSource={site74} columns={columns} pagination={{ pageSize: 100 }} scroll={{ y: 480 }} />
        </TabPane>
        <TabPane tab="N122" key="4">
            <Table dataSource={site122} columns={columns} pagination={{ pageSize: 100 }} scroll={{ y: 480 }} />
        </TabPane>
        <TabPane tab="N149" key="5">
            <Table dataSource={site149} columns={columns} pagination={{ pageSize: 100 }} scroll={{ y: 480 }} />
        </TabPane>
        <TabPane tab="N165" key="6">
            <Table dataSource={site165} columns={columns} pagination={{ pageSize: 100 }} scroll={{ y: 480 }} />
        </TabPane>
        <TabPane tab="N234" key="7">
            <Table dataSource={site234} columns={columns} pagination={{ pageSize: 100 }} scroll={{ y: 480 }} />
        </TabPane>
        <TabPane tab="N282" key="8">
            <Table dataSource={site282} columns={columns} pagination={{ pageSize: 100 }} scroll={{ y: 480 }} />
        </TabPane>
        <TabPane tab="N331" key="9">
            <Table dataSource={site331} columns={columns} pagination={{ pageSize: 100 }} scroll={{ y: 480 }} />
        </TabPane>
        <TabPane tab="N343" key="10">
            <Table dataSource={site343} columns={columns} pagination={{ pageSize: 100 }} scroll={{ y: 480 }} />
        </TabPane>
        <TabPane tab="N603" key="11">
            <Table dataSource={site603} columns={columns} pagination={{ pageSize: 100 }} scroll={{ y: 480 }} />
        </TabPane>
        <TabPane tab="N616" key="12">
            <Table dataSource={site616} columns={columns} pagination={{ pageSize: 100 }} scroll={{ y: 480 }} />
        </TabPane>

        <TabPane tab="N657" key="13">
            <Table dataSource={site657} columns={columns} pagination={{ pageSize: 100 }} scroll={{ y: 480 }} />
        </TabPane>
        <TabPane tab="N709" key="14">
            <Table dataSource={site709} columns={columns} pagination={{ pageSize: 100 }} scroll={{ y: 480 }} />
        </TabPane>
        <TabPane tab="N717" key="15">
            <Table dataSource={site717} columns={columns} pagination={{ pageSize: 100 }} scroll={{ y: 480 }} />
        </TabPane>
        <TabPane tab="N801" key="16">
            <Table dataSource={site801} columns={columns} pagination={{ pageSize: 100 }} scroll={{ y: 480 }} />
        </TabPane>
        <TabPane tab="N1074" key="17">
            <Table dataSource={site1074} columns={columns} pagination={{ pageSize: 100 }} scroll={{ y: 480 }} />
        </TabPane>

        <TabPane tab="N1098" key="18">
            <Table dataSource={site1098} columns={columns} pagination={{ pageSize: 100 }} scroll={{ y: 480 }} />
        </TabPane>
        <TabPane tab="N1134" key="19">
            <Table dataSource={site1134} columns={columns} pagination={{ pageSize: 100 }} scroll={{ y: 480 }} />
        </TabPane>
        <TabPane tab="N1158" key="20">
            <Table dataSource={site1158} columns={columns} pagination={{ pageSize: 100 }} scroll={{ y: 480 }} />
        </TabPane>
        <TabPane tab="N1173" key="21">
            <Table dataSource={site1173} columns={columns} pagination={{ pageSize: 100 }} scroll={{ y: 480 }} />
        </TabPane>
        <TabPane tab="N1194" key="22">
            <Table dataSource={site1194} columns={columns} pagination={{ pageSize: 100 }} scroll={{ y: 480 }} />
        </TabPane>
    </Tabs>

      </div>
      </Content>
    <FooterPage /> 
     </Layout>
  )
}

export async function getServerSideProps() {
    // Fetch data from external API
    const res = await fetch(`https://storage.googleapis.com/data.unicarbkb.org/covid/prod/crispin_covid19.json`)
    const data = await res.json()
  
    // Pass data to the page via props
    return { props: { data } }
  }

export default sarscov2