import React, { useCallback } from 'react';
import HeaderPage from '../components/HeaderPage';

declare global {
    namespace JSX {
        interface IntrinsicElements {
            'my-html-custom-tag': React.DetailedHTMLProps<React.HTMLAttributes<HTMLElement>, HTMLElement>;
            'protvista-datatable': any;
        }
    }
}


function Proteinviewtest() {

    const columns = {
        column1: {
          label: "My first column",
          resolver: d => d["chellos"] //this is used to resolve what to display in the column
        }
      };

      //const data = ["jsjsj"];

      var data2 = { 
        firstname:"Tom", 
        d:"Hanks" 
     };
      
      const setTableData = useCallback(
        (node): void => {
          if (node) {
            // eslint-disable-next-line no-param-reassign
            node.data = data2;
            // eslint-disable-next-line no-param-reassign
            node.columns = columns;
          }
        },
        [data2]
      );

    return (
        <div>
          <HeaderPage />
          <p>Hello world!</p>
          <script src="http://ebi-uniprot.github.io/CDN/protvista/protvista.js"></script>
<link href="http://ebi-uniprot.github.io/CDN/protvista/css/main.css" rel="stylesheet"/>


          <script src="https://cdn.jsdelivr.net/npm/d3@5.9.2/dist/d3.min.js" defer></script>
          <script src="https://cdn.jsdelivr.net/npm/protvista-utils@latest/dist/protvista-utils.js" defer ></script>
    <script src="https://cdn.jsdelivr.net/npm/data-loader@latest/dist/data-loader.js" defer></script>
    <script src="https://cdn.jsdelivr.net/npm/protvista-feature-adapter@latest/dist/protvista-feature-adapter.js" defer></script>
    <script src="https://cdn.jsdelivr.net/npm/protvista-zoomable@latest/dist/protvista-zoomable.js" defer ></script>
    <script src="https://cdn.jsdelivr.net/npm/protvista-track@latest/dist/protvista-track.js" defer></script>


  
    <protvista-datatable id="data-table" ref={setTableData} />
    

         </div>
      )
    }

  export default Proteinviewtest;
  