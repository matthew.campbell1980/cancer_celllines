//
import React, { useEffect, Component } from "react";
import { Table, Layout, Breadcrumb, Alert } from 'antd';
import { Head } from "next/document";
import Content from 'antd/lib/layout';
import { Typography } from 'antd';
import {Helmet} from "react-helmet";
//import {HelmetProvider, Helmet } from 'react-helmet-async'
import HeaderPage from "../components/HeaderPage";
import FooterPage from "../components/FooterPage";
import GlygenNote from "../components/GlygenNote";

const { Title, Text } = Typography;


let s = React.createElement("script", {
    src: "https://unpkg.com/@triply/yasgui/build/yasgui.min.js",
    async: false,
    defer: false,
    crossOrigin: "anonymous"
  })

  let x = ' const yasgui = new Yasgui(document.getElementById("yasgui"), { '+
    'requestConfig: { endpoint: "http://203.101.226.128:40935/unicarbkb/query" }, ' +
    ' copyEndpointOnNewTab: true  } ' +
    ' }); ';

class Sparql extends Component {

    

  render() {
    let s = "const yasgui = new Yasgui(document.getElementById('yasgui'), { requestConfig: { endpoint: 'http://203.101.226.128:40935/unicarbkb/query' }, copyEndpointOnNewTab: true});";

  return (
    <Layout className="layout">
    <HeaderPage />
    <Content style={{ padding: '0 50px', marginTop: 20 }}>
   <Breadcrumb style={{ margin: '24px 0' }}>
     <Breadcrumb.Item>Home</Breadcrumb.Item>
     <Breadcrumb.Item>SPARQL Endpoint</Breadcrumb.Item>
   </Breadcrumb>
   <div style={{ background: '#fff', padding: 24 }}>
     <Title level={2}>UniCarbKB SPARQL</Title>
     <Title level={4}><a href="https://app.gitbook.com/@unicarbkb-glycostore/s/data/">GitBook Examples</a></Title>
     <GlygenNote />
      
    <div>
       
        <link href="https://unpkg.com/@triply/yasgui/build/yasgui.min.css" rel="stylesheet" type="text/css" />
        <script src="https://unpkg.com/@triply/yasgui/build/yasgui.min.js"></script>
        <div id="yasgui"></div>
        <Helmet><script>{s}</script></Helmet>

       
    </div>
    </div>

    </Content>
    <FooterPage />
      </Layout>
        
    




  )
  }

}

export default Sparql;