import React, { useState, constructor } from 'react';
import fetch from 'isomorphic-unfetch';
import { Table, Tag,  Tabs, Descriptions, Tooltip, message, Button, Avatar, List } from 'antd';
import {  Breadcrumb } from 'antd';
import { Typography } from 'antd';

const { Text } = Typography;
import window from 'global'


const { Title } = Typography;

import Layout from 'antd/lib/layout';
import Content from 'antd/lib/layout';
import FooterPage from '../components/FooterPage';
import HeaderPage from '../components/HeaderPage';
import Link from 'next/link';
import Paragraph from 'antd/lib/typography/Paragraph';
import { FormatAlignJustifyTwoTone } from '@material-ui/icons';

const { TabPane } = Tabs;


const columns = [
  {
    title: 'Image',
    dataIndex: 'image',
    key: 'image',
    width: '40%',
    fixed: true,
    render: (text) => <img src={"https://storage.googleapis.com/glycostore_reducedend/production/resize/lg-snfg-GS" + text + ".png" }  />  
  },
  {
    title: 'Glycan',
    dataIndex: 'glycan',
    key: 'glycan',
    width: '20%',
    sorter: true,
  },
  {
    title: 'Mass',
    dataIndex: 'mass',
    key: 'mass',
    width: '10%'
  },
  {
    title: 'Rel. Area (%)',
    dataIndex: 'relarea',
    key: 'relarea',
    width: '10%'
  },
  {
    title: 'RT',
    dataIndex: 'rt',
    key: 'rt',
    width: '5%' 
  },
  {
    title: 'MS2',
    dataIndex: 'ms2',
    key: 'ms2',
    width: '5%',
    render: (ms2) =>  <a href={"http://www.glycostore.org/showMelanomaGlycan/" + ms2}>MS2</a>  
    
    //render: (ms2) =>  { if(ms2 === undefined && typeof ms2 == 'undefined') {<a href={"rubbish"}>crap</a>} else {

    // <a href={"http://www.glycostore.org/showMelanomaGlycan/" + ms2}>MS2</a>}
    //}
    
    //< a href={"http://www.glycostore.org/showMelanomaGlycan/" + ms2}>MS2</a>

    
  }/*,
  {
    title: 'Id',
    dataIndex: 'gs',
    key: 'gs',
    width: '5%'
  }*/
  /*,
  {
    title: 'GlyToucan',
    dataIndex: 'glytoucan',
    key: 'glytoucan',
    width: '10%'
  }*/
];

const data = [];
const ifg2 = [];
const ifg3 = [];
const ifg4 = [];
const i7724 = [];
const i71 = [];
const i69_04061 = [];
const i66_040619 = [];
const i040619 = [];
const tcp13Data = [];
const tcp14Data = [];
const melanomaData = [];


function callback(key) {
  console.log(key);
}


function Index({ json, json2, json3, json4, json5, json6 , json7, json8, json9, jsonTCP13, jsonTCP14, jsonCRL7724, jsonHTB66, jsonHTB69, jsonHTB71, melanomaJson  }) {

  
    //return <div>Next stars: {stars} and test: {test} 
    //<div>works</div> </div>    
    //json.melanoma.cart_items.map( t => data.push( { key:t.rt+1 , relarea: (t.relarea*100).toFixed(2) , glycan: t.glycan, glytoucan: t.glytoucan, rt: t.rt, mass: t.mass , gs: t.gs, image: t.gs, ms:t.ms} ) ) ;
   
    //json2.melanoma.cart_items.map( t2 => ifg2.push( { key:t2.rt+1 , relarea: (t2.relarea*100).toFixed(2) , glycan: t2.glycan, glytoucan: t2.glytoucan, rt: t2.rt, mass: t2.mass , gs: t2.gs, image: t2.gs, ms:t2.ms } ) ) ;
    //json3.melanoma.cart_items.map( t3 => ifg3.push( { key:t3.rt+1 , relarea: (t3.relarea*100).toFixed(2) , glycan: t3.glycan, glytoucan: t3.glytoucan, rt: t3.rt, mass: t3.mass , gs: t3.gs, image: t3.g, ms:t3.ms, } ) ) ;
    //json4.melanoma.cart_items.map( t4 => ifg4.push( { key:t4.rt+1 , relarea: (t4.relarea*100).toFixed(2) , glycan: t4.glycan, glytoucan: t4.glytoucan, rt: t4.rt, mass: t4.mass , gs: t4.gs, image: t4.gs, ms:t4.ms } ) ) ;
    json5.melanoma.cart_items.map( t5 => i7724.push( { key:t5.rt+1 , relarea: (t5.relarea*100).toFixed(2) , glycan: t5.glycan, glytoucan: t5.glytoucan, rt: t5.rt, mass: t5.mass , gs: t5.gs, image: t5.gs, ms:t5.ms,  ms2:t5.ms2 } ) ) ;
    json6.melanoma.cart_items.map( t6 => i71.push( { key:t6.rt+1 , relarea: (t6.relarea*100).toFixed(2) , glycan: t6.glycan, glytoucan: t6.glytoucan, rt: t6.rt, mass: t6.mass , gs: t6.gs, image: t6.gs, ms:t6.ms, ms2:t6.ms2 } ) ) ;
    json7.melanoma.cart_items.map( t7 => i69_04061.push( { key:t7.rt+1 , relarea: (t7.relarea*100).toFixed(2) , glycan: t7.glycan, glytoucan: t7.glytoucan, rt: t7.rt, mass: t7.mass , gs: t7.gs, image: t7.gs, ms:t7.ms, ms2:t7.ms2 } ) ) ;
   
    json8.melanoma.cart_items.map( t8 => i66_040619.push(  { 

      

      key:t8.rt+1 , relarea: (t8.relarea*100).toFixed(2) , glycan: t8.glycan, glytoucan: t8.glytoucan, rt: t8.rt, mass: t8.mass , gs: t8.gs, image: t8.gs, ms:t8.ms, ms2: t8.ms2
    
    } )
    
    )//.map( x8 => i66_040619.push( { ms22:"shshssh"}) ) ;
    
    
    //console.log("DJJDJDJDJJDJDJ" + JSON.stringify(i66_040619) )
    
    
    
    
    json9.melanoma.cart_items.map( t9 => i040619.push( { key:t9.rt+1 , relarea: (t9.relarea*100).toFixed(2) , glycan: t9.glycan, glytoucan: t9.glytoucan, rt: t9.rt, mass: t9.mass , gs: t9.gs, image: t9.gs, ms:t9.ms, ms2:t9.ms2 } ) ) ;
   
    jsonTCP13.melanoma.cart_items.map( t10 => tcp13Data.push( { name: t10.name  , atcc: t10.atcc , cellosaurus: t10.cellosaurus } ) ); 
    jsonTCP14.TCP1014._cell.map( t11 => tcp14Data.push( { name: t11.name  , atcc: t11.atcc , cellosaurus: t11.cellosaurus } ) ); 

    data.sort(function (a, b) {
      return a.rt - b.rt;
    });

    ifg2.sort(function (a, b) {
      return a.rt - b.rt;
    });

    ifg3.sort(function (a, b) {
      return a.rt - b.rt;
    });

    ifg4.sort(function (a, b) {
      return a.rt - b.rt;
    });

    i7724.sort(function (a, b) {
      return a.rt - b.rt;
    });

    i71.sort(function (a, b) {
      return a.rt - b.rt;
    });

    i69_04061.sort(function (a, b) {
      return a.rt - b.rt;
    });

    i66_040619.sort(function (a, b) {
      return a.rt - b.rt;
    });

    i040619.sort(function (a, b) {
      return a.rt - b.rt;
    });

   //tcp14

    melanomaJson.cellPublications.items.map( t10 => melanomaData.push( 
      { title: t10.title , pmid: t10.pmid, pubmed: "https://www.ncbi.nlm.nih.gov/pubmed/"+t10.pmid,
        year: t10.year, authors: t10.authors, journal: t10.journal, pages: t10.pages,
        description: t10.authors + " , " + t10.journal + " , " + t10.year + " PMID: " +  t10.pmid,
      cover:  t10.cover} 
      ) );

      melanomaJson.cellPublications.items.map( t10 => melanomaData.push( 
        { mass: t10.instruments } ));
    



    const gradient = [
      {
        message: 'Gradient: (90 min) 99% A for 16 min, 15.8-40% B (16.1-67 min), 40-90% B (67-70 min), 90% B (70-75 min), 99% A (76-83 min) 7 min post run 99% A',
      }
    ];

  

    return (
      <Layout className="layout">
    <HeaderPage />
    <Content style={{ background: 'ghostwhite', padding: '0 50px' }}>
      <Breadcrumb style={{ margin: '16px 0' }}>
        <Breadcrumb.Item>UniCarbKB-GlyGen</Breadcrumb.Item>
        <Breadcrumb.Item>Cell Line</Breadcrumb.Item>
        <Breadcrumb.Item>Melanoma</Breadcrumb.Item>
      </Breadcrumb>
      <div className="site-layout-content"> 
      <Typography>
      <Title level={2}>Glycome Profile Melanoma Cell Lines</Title>
      <Descriptions title="" layout="horizontal" column={1}>
        <Descriptions.Item label="Project">Glycome profiling of established Melanoma Cell Lines and IFG Cell Bank</Descriptions.Item>
        <Descriptions.Item label="Subjects">


          <Tag color="#d20962"><Tooltip title="Ontology: http://purl.obolibrary.org/obo/NCIT_C153192"><span>Glycomics</span></Tooltip></Tag>
          <Tag color="#d20962"><Tooltip title="Ontology: http://purl.obolibrary.org/obo/NCIT_C18816"><span>Glycobiology</span></Tooltip></Tag> 
          <Tag color="#d20962"><Tooltip title="Ontology: http://purl.obolibrary.org/obo/NCIT_C44282"><span>Cell Line</span></Tooltip></Tag> 
          
        
        </Descriptions.Item>
        <Descriptions.Item label="Instrument">
          <Tag color="#00a78e"><Tooltip title="Ontology: http://purl.obolibrary.org/obo/MS_1002301"><span>Bruker amaZon Speed</span></Tooltip></Tag>
          <Tag color="#00a78e"><Tooltip title="Ontology: not available"><span>Thermo Ultimate 3000</span></Tooltip></Tag> 
        </Descriptions.Item>
        <Descriptions.Item label="Cell Line">
          {tcp14Data.map((item) =>
            <Tag color="#00bce4"><span>{item.name} (ATCC: {item.atcc}) <a href='https://web.expasy.org/cellosaurus/{item.cellosaurus}'>{item.cellosaurus}</a></span></Tag>
          )}
        </Descriptions.Item>
      
        <Title>Methods</Title>
      
        <Paragraph>Glycans were enzymatically released and characterised using a Porous Graphitized Carbon Liquid Chromatography Mass spectrometry glycomics platform. 
        Structures were fully assigned using MS/MS fragmentation patterns, PGC retention behaviours, and monosaccharide linkages confirmed using an array of exoglycosidase enzymes.
          Experimental Protocol described Abrahams JL et al., Building a PGC-LC-MS N-glycan retention library and elution mapping resource. 
          </Paragraph> 
          <Paragraph>
          <ul><li>
            <Link href="https://doi.org/10.1007/s10719-017-9793-4">DOI: 10.1007/s10719-017-9793-4</Link>
            </li></ul>
        </Paragraph>
        <List
  
    dataSource={gradient}
    renderItem={item => (
      <Paragraph>
        Running Conditions
          {item.message}
      </Paragraph>
    )}
  />
        <Descriptions.Item label="Running">
        <Tag color="#00bce4"><span>Gradient: 90 min</span></Tag>
        <Tag color="#00bce4"><span>Flow Rate: 1 uL/min</span></Tag>
        <Tag color="#00bce4"><span>Injection: 3 µl (8 µl injection loop)</span></Tag>
        <Tag color="#00bce4"><span>A: 10 mM Ammonium bicarbonate</span></Tag>
        <Tag color="#00bce4"><span>B: 70% Acetonitrile in 10 mM Ammonium bicarbonate</span></Tag>
        <br/>
        <Tag color="#00bce4"><span>Column: Thermo Hypercarb KAPPA 100x0.18mm 5um</span></Tag>
        <br/>
        </Descriptions.Item>
      </Descriptions>
      <Title level={4}>Data Collection</Title>
 </Typography>
      <Tabs  onChange={callback}>
        <TabPane tab="Glycome Dashboard" key="9999">
          <Content><div>
          <div dangerouslySetInnerHTML={{
    __html: "<div class='tableauPlaceholder' id='viz1636050194246' style='position: relative'><noscript><a href='#'><img alt='Melanoma Glycomics Cell Line Comparison ' src='https:&#47;&#47;public.tableau.com&#47;static&#47;images&#47;W4&#47;W4QFJPFQD&#47;1_rss.png' style='border: none' /></a></noscript><object class='tableauViz'  style='display:none;'><param name='host_url' value='https%3A%2F%2Fpublic.tableau.com%2F' /> <param name='embed_code_version' value='3' /> <param name='path' value='shared&#47;W4QFJPFQD' /> <param name='toolbar' value='yes' /><param name='static_image' value='https:&#47;&#47;public.tableau.com&#47;static&#47;images&#47;W4&#47;W4QFJPFQD&#47;1.png' /> <param name='animate_transition' value='yes' /><param name='display_static_image' value='yes' /><param name='display_spinner' value='yes' /><param name='display_overlay' value='yes' /><param name='display_count' value='yes' /><param name='language' value='en-GB' /></object></div>                <script type='text/javascript'>                    var divElement = document.getElementById('viz1636050194246');                    var vizElement = divElement.getElementsByTagName('object')[0];                    vizElement.style.width='1366px';vizElement.style.height='795px';                    var scriptElement = document.createElement('script');                    scriptElement.src = 'https://public.tableau.com/javascripts/api/viz_v1.js';                    vizElement.parentNode.insertBefore(scriptElement, vizElement);                </script>"
  }} />



            </div>
          </Content>
        


        
        </TabPane>
        
        <TabPane tab="ATCC: CRL-7724" key="5">
      
          <Text type="secondary">
          Cell Line Info. - ATCC: {jsonCRL7724.CRL7724.atcc}, Name: {jsonCRL7724.CRL7724.name}, <a  target="_blank" rel="noopener noreferrer" href={ jsonCRL7724.CRL7724.cellosaurus }>Cellosaurus</a>
          </Text>
          <Table dataSource={i7724} columns={columns} pagination={{ pageSize: 100 }} scroll={{ y: 480 }} />
        </TabPane>
        <TabPane tab="ATCC: HTB-71" key="6">
          <Text type="secondary">
          Cell Line Info. -ATCC: {jsonHTB71.HTB71.atcc}, Name: {jsonHTB71.HTB71.name}, <a  target="_blank" rel="noopener noreferrer" href={ jsonHTB71.HTB71.cellosaurus }>Cellosaurus</a>
          </Text>
          <Table dataSource={i71} columns={columns} pagination={{ pageSize: 100 }} scroll={{ y: 480 }} />
        </TabPane>
        <TabPane tab="ATCC: HTB-69" key="7">
          <Text type="secondary">
          Cell Line Info. -ATCC: {jsonHTB69.HTB69.atcc}, Name: {jsonHTB69.HTB69.name}, <a  target="_blank" rel="noopener noreferrer" href={ jsonHTB69.HTB69.cellosaurus }>Cellosaurus</a>
          </Text>
          <Table dataSource={i69_04061} columns={columns} pagination={{ pageSize: 100 }} scroll={{ y: 480 }} />
        </TabPane>
        <TabPane tab="ATCC: HTB-66" key="8">
          <Text type="secondary">
          Cell Line Info. -ATCC: {jsonHTB66.HTB66.atcc}, Name: {jsonHTB66.HTB66.name}, <a target="_blank" rel="noopener noreferrer" href={ jsonHTB66.HTB66.cellosaurus }>Cellosaurus</a>
          </Text>
          <Table dataSource={i66_040619} columns={columns} pagination={{ pageSize: 100 }} scroll={{ y: 480 }} />
        </TabPane>
      
        </Tabs> 
      </div>
    </Content>
    <FooterPage /> 
     </Layout>

  


    )
  }
  
  Index.getInitialProps = async () => {
    //const res = await fetch('https://api.github.com/repos/zeit/next.js');
    //older swift https://swift.rc.nectar.org.au/v1/AUTH_1507c81fb388480f88312a2d2f9189cd/nextjstest/
    /*const res = await fetch('https://storage.googleapis.com/melanoma_celllines/IFG01_11032020.json');
    const json = await res.json();

    const res2 = await fetch('https://storage.googleapis.com/melanoma_celllines/IFG02.json');
    const json2 = await res2.json();

    const res3 = await fetch('https://storage.googleapis.com/melanoma_celllines/IFG03.json');
    const json3 = await res3.json();

    const res4 = await fetch('https://storage.googleapis.com/melanoma_celllines/IFG04.json');
    const json4 = await res4.json();*/

    const res5 = await fetch('https://storage.googleapis.com/melanoma_celllines/7724.json'); ///start
    const json5 = await res5.json();

    const res6 = await fetch('https://storage.googleapis.com/melanoma_celllines/71.json'); //start
    const json6 = await res6.json();

    const res7 = await fetch('https://storage.googleapis.com/melanoma_celllines/69_040619.json'); //start
    const json7 = await res7.json();

    //const res8 = await fetch('https://storage.googleapis.com/melanoma_celllines/66_040619.json'); //start
    //const json8 = await res8.json();

    const res8 = await fetch('https://storage.googleapis.com/melanoma_celllines/testing.json'); //start
    const json8 = await res8.json();

    const res9 = await fetch('https://swift.rc.nectar.org.au/v1/AUTH_1507c81fb388480f88312a2d2f9189cd/nextjstest/040619.json');
    const json9 = await res9.json();
  

    const tcp13 = await fetch('https://storage.googleapis.com/glycan_cell_panel/TCP1013.json');
    const jsonTCP13 = await tcp13.json();


    const tcp14 = await fetch('https://storage.googleapis.com/glycan_cell_panel/TCP1014.json');
    const jsonTCP14 = await tcp14.json();

    const CRL7724 = await fetch('https://storage.googleapis.com/glycan_cell_panel/CRL7724.json');
    const jsonCRL7724 = await CRL7724.json();

    const HTB66 = await fetch('https://storage.googleapis.com/glycan_cell_panel/HTB66.json');
    const jsonHTB66 = await HTB66.json();

    const HTB69 = await fetch('https://storage.googleapis.com/glycan_cell_panel/HTB69.json');
    const jsonHTB69 = await HTB69.json();

    const HTB71 = await fetch('https://storage.googleapis.com/glycan_cell_panel/HTB71.json');
    const jsonHTB71 = await HTB71.json();

    const melanoma = await fetch('https://storage.googleapis.com/journal-image/cellLinePublications.json'); //CVCL_0026.json');
    const melanomaJson = await melanoma.json();
    
    //return { stars: json.size , test: json.watchers_count };

    console.log("check this out: " + JSON.stringify(json8) )

    
    //return { json, json2, json3, json4, json5, json6, json7, json8, json9, jsonTCP13, jsonTCP14, jsonCRL7724, jsonHTB66, jsonHTB69, jsonHTB71, melanomaJson  };
    return {  json5, json6, json7, json8, json9, jsonTCP13, jsonTCP14, jsonCRL7724, jsonHTB66, jsonHTB69, jsonHTB71, melanomaJson  };

  };


  
  export default  Index;
  