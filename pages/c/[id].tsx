import React, { useState, useEffect, Fragment, forwardRef } from 'react';
import { useRouter } from 'next/router'
import fetch from 'isomorphic-unfetch';
import { Table, Row, Col, Tag,  PageHeader, Tabs, Button, Statistic, Descriptions, Avatar, Tooltip, AutoComplete, Menu, Breadcrumb, Divider, List } from 'antd';

const { Footer } = Layout;
import Layout from 'antd/lib/layout';

import Content from 'antd/lib/layout';

import { inspect } from 'util'
import Header from 'antd/lib/calendar/Header';
import Title from 'antd/lib/skeleton/Title';

const { TabPane } = Tabs;

const publicationsData = [
  {
    title: 'Ant Design Title 1',
  },
  {
    title: 'Ant Design Title 2',
  },
  {
    title: 'Ant Design Title 3',
  },
  {
    title: 'Ant Design Title 4',
  },
];



const data = [];

function getQuery(){
  const router = useRouter()
  const { id } = router.query
  console.log("id check " + id)
}

function callback(key) {
  console.log(key);
}


const ref = React.createRef();

function Rename({ json, id  }) {

   
  
    //return <div>Next stars: {stars} and test: {test} 
    //<div>works</div> </div>    
    console.log("testing json " + json.OX  );
  

    return (
      <Layout className="layout">
   
    <Content style={{ padding: '0 50px' }}>
      <Breadcrumb style={{ margin: '16px 0' }}>
        <Breadcrumb.Item>UniCarbKB-GlyGen</Breadcrumb.Item>
        <Breadcrumb.Item>Cell Line</Breadcrumb.Item>
        <Breadcrumb.Item>{json.ID} - {json.AC} </Breadcrumb.Item>
      </Breadcrumb>
      <div style={{ background: '#fff', padding: 20, minHeight: 280 }}> 
       <Descriptions title="" layout="horizontal" column={1}>
        <Descriptions.Item label="Project">Glycome profiling of established {json.ID} Cell Lines</Descriptions.Item>
        <Descriptions.Item label="Subjects">
          <Tag color="orange"><Tooltip title="Ontology: http://purl.obolibrary.org/obo/NCIT_C44282"><span>{json.DI}</span></Tooltip></Tag> 
          <Tag color="orange"><Tooltip title="Ontology: http://purl.obolibrary.org/obo/NCIT_C44282"><span>{json.OX}</span></Tooltip></Tag> 
        </Descriptions.Item>
        <Descriptions.Item label="Subjects">
          <Tag color="blue"><Tooltip title="Ontology: http://purl.obolibrary.org/obo/NCIT_C18816"><span>{json.SX}</span></Tooltip></Tag> 
          <Tag color="blue"><Tooltip title="Ontology: http://purl.obolibrary.org/obo/NCIT_C18816"><span>{json.AG}</span></Tooltip></Tag> 
        </Descriptions.Item>
      </Descriptions>
      <Divider orientation="left">Publications</Divider>
      <List itemLayout="horizontal" dataSource={publicationsData} renderItem={item => (
        <List.Item>
        <List.Item.Meta
          avatar={<Avatar src="https://storage.googleapis.com/journal-image/mcp.png" />}
          title={<a href="https://ant.design">{item.title}</a>}
          description="Ant Design, a design language for background applications, is refined by Ant UED Team"
        />
        </List.Item>
      )} />

      <div><Avatar size="large" src="https://storage.googleapis.com/journal-image/mcp.png" /><p>testing this sentence ofr now to add</p></div>
      </div>
    </Content>
    <Footer style={{ textAlign: 'center' }}>Supported by the Institute for Glycomics, UniCarbKB, Australian Research Data Commons and Glygen.</Footer>
  </Layout>

  


    )
  }
  
  Rename.getInitialProps = async function ( context ) {
    //const res = await fetch('https://api.github.com/repos/zeit/next.js');
    const { id } = context.query;
    //const res = await fetch('https://storage.googleapis.com/cellosaurus/CVCL_0026.json');
    const res = await fetch('https://storage.googleapis.com/cellosaurus/' + id + '.json' ); //CVCL_0026.json');
    const json = await res.json();


    console.log("inspection check " + inspect(id));
    //return { stars: json.size , test: json.watchers_count };
    return { json, id };
  };


  
  export default Rename;
  