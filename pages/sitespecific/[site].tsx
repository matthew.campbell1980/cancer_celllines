import fetch from 'isomorphic-unfetch';
import { Layout, Breadcrumb, Typography, Row, Col, Statistic, List, Avatar, Table } from 'antd';
import { BookOutlined } from '@ant-design/icons';
import Content from 'antd/lib/layout';
import HeaderPage from '../../components/HeaderPage';
import FooterPage from '../../components/FooterPage';
import Link from 'next/link';
import GlygenNote from '../../components/GlygenNote';
import Comps from '../../components/Comps';
import Strs from '../../components/Strs';
import { Carousel } from 'antd';
const { Title, Text } = Typography;


function TestSiteSpecific({ uniprot, position, pmids, pmidCount, comp, compCount,  structure, strCount }) {

    for(let c of comp){
        console.log("88819dhdhdhjsk  " + c);
    }

    const columns = [
        {
            title: 'Image',
            dataIndex: 'image',
            key: 'image',
            width: '30%',
            fixed: true,
            render: (text, record) => <img src={"https://swift.rc.nectar.org.au/v1/AUTH_c68cac76b84547d58ce4dc089f5f5844/compositions/" + text + ".png" }  /> 
          }
          
        ];

    return (
        <Layout className="layout">
            <HeaderPage />
            <Content style={{ padding: '0 50px', marginTop: 20 }}>
                <Breadcrumb style={{ margin: '24px 0' }}>
                    <Breadcrumb.Item>Home</Breadcrumb.Item>
                    <Breadcrumb.Item>Glycoprotein </Breadcrumb.Item>
                    <Breadcrumb.Item>{uniprot} - Position {position} </Breadcrumb.Item>
                </Breadcrumb>
                <div style={{ background: '#fff', padding: 24 }}>
                    <Title level={2}>{uniprot}: Position {position}</Title>
                    <GlygenNote />
                    <Row gutter={16}>
                        <Col span={6}>
                            <Statistic title="Curated Manuscripts" value={pmidCount} prefix={<BookOutlined />} />
                        </Col>
                        <Col span={18}>
                            <List
                                itemLayout="horizontal"
                                dataSource={pmids}
                                renderItem={item => (
                                    <List.Item>
                                        <List.Item.Meta
                                            
                                            title={<a href="https://ant.design">{pmids}</a>}
                                            description="Ant Design, a design language for background applications, is refined by Ant UED Team"
                                        />
                                    </List.Item>
                                )} />
                        </Col>
                    </Row>
                    <Title level={3}>Structures</Title>
                    <Comps style="max-width: 30px" compsCount={compCount} /><br />
                    <Strs strCount={strCount} />

                    <Table  rowKey="accession" columns={columns} dataSource={ comp }  />
                    

                    
                    
                    <ol>
      {comp.map(c => <li><img src={"https://swift.rc.nectar.org.au/v1/AUTH_c68cac76b84547d58ce4dc089f5f5844/compositions/" + c.trim() + ".png" } /></li>)}


    </ol>

   
    

                </div>
            </Content>
            <FooterPage />
        </Layout>

    )

}



TestSiteSpecific.getInitialProps = async function (context) {

    const { query } = context;
    console.log("query: " + query)

    const accession = query.site;
    const uniprot = accession.split("_", 1)
    const position = accession.split("_", 2)[1]


    const res = await fetch('https://storage.googleapis.com/unicarbkb_glycoproteins/test.json');
    const json = await res.json();

    const uniprotString = JSON.stringify(uniprot).replace("\"", "").replace("\[", "").replace("\]", "").replace("\"", "");
    const positionString = JSON.stringify(position).replace("\"", "").replace("\[", "").replace("\]", "").replace("\"", "");
    const data = json.filter(t => t.identifer === uniprotString).filter(t => t.Position === positionString + '^^xsd:int')//.filter(t => t.Id.match(/comp/))
    console.log("data " + data.identifer)
    for (let x of data) {
        console.log("data " + x.identifer)
    }
    const shit = uniprotString


    const pmid = JSON.stringify(data.map(t10 => t10.Pmid))
    const sts = JSON.stringify(data.map(t10 => t10.Id))

    const x: String = data.map(t10 => t10.Pmid)
    
    const strs = JSON.stringify(data.map(t10 => t10.Id))
    console.log("strs.... " + strs);
    const strsId = [] = strs.replace("\"", "").replace("\[", "").replace("\]", "").replace("\"", "").split(",");

    const comp = []
    const structure = []
    for(let s of strsId){
       
        if(s.match(/comp/)){
            comp.push(s)
            console.log("str id: " + s.trim())
        } else {
            structure.push(s.trim)
        }
    }
    let compCount = comp.length
    let strCount = structure.length
    
    const pmids = [] = pmid.replace("\"", "").replace("\[", "").replace("\]", "").replace("\"", "").split(",");
    let pmidCount = pmids.length;

  


    //uniprotString = JSON.stringify(uniprot).replace("\"", "").replace("\[", "").replace("\]", "").replace("\"", "");
    //const positionString = JSON.stringify(position)//.replace("\"", "").replace("\[", "").replace("\]", "").replace("\"", "");



    console.log("shshs", position)
    return { uniprot, position, pmids, pmidCount, comp, compCount, structure, strCount}

}

export default TestSiteSpecific;