import { Table, Layout, Breadcrumb, Alert, Avatar, List, Skeleton, Row, Col } from 'antd';
import Content from 'antd/lib/layout';
import { Typography } from 'antd';
import fetch from 'isomorphic-unfetch';
import HeaderPage from '../../components/HeaderPage';
import FooterPage from '../../components/FooterPage';
import GlygenNote from '../../components/GlygenNote';
import Comps from '../../components/Comps';
import Strs from '../../components/Strs';
import Link from 'next/link';
import { Badge } from 'antd';
import Test from '../test';
const { Title, Text } = Typography;


const columns = [
    {
        title: 'GlyTouCan (GlyGen Link)',   
        dataIndex: 'toucan',
        width: '20%',
        render: text => <div><a href={"https://www.glygen.org/glycan_detail.html?glytoucan_ac=" + text  }> {text} </a> <br/><br /><a target="_blank" rel="noopener noreferrer" href={"https://raw.githack.com/glygen-glycan-data/GNOme/GlyGen_DEV/restrictions/GNOme_GlyGen.browser.html?focus=" + text }>Related Structures</a></div>
    },
    {
      title: 'Image',   
      dataIndex: 'toucan',
      width: '15%',
      fixed: false,
      render: (text) => <img object-fit="cover" height="220"  src={"https://api.glygen.org/glycan/image/" + text  }  />  
      //<img height="42" width="42" src={"https://swift.rc.nectar.org.au/v1/AUTH_c68cac76b84547d58ce4dc089f5f5844/compositions/" + text + ".png" }  /> 
    },

    {
      title: 'HexNAc',   
      dataIndex: 'hexnac',
      width: '10%',
    },
    {
      title: 'Hex',   
      dataIndex: 'hex',
      width: '10%',
    },
    {
      title: 'dHex',   
      dataIndex: 'dhex',
      width: '10%',

    },
    {
      title: 'NeuAc',   
      dataIndex: 'neuac',
      width: '10%', 
    },
    {
      title: 'NeuGc',   
      dataIndex: 'neugc',
      width: '10%',
    },
    {
      title: 'S',   
      dataIndex: 's',
      width: '5%', 
    },
    {
      title: 'P',   
      dataIndex: 'p',
      width: '5%',
    }
  ]


function Composition({data, unique, toucans, composition, massMono, massAverage, compositionTrim}) {

   
    for(let q of unique){
        console.log("sjsjsjoaosk " + q)
    
    }

    return(
        <Layout className="layout">
       <HeaderPage />
       <Content style={{ padding: '0 50px', marginTop: 20 }}>
      <Breadcrumb style={{ margin: '24px 0' }}>
        <Breadcrumb.Item>Home</Breadcrumb.Item>
        <Breadcrumb.Item>Composition</Breadcrumb.Item>
      </Breadcrumb>
      <div style={{ background: '#fff', padding: 24 }}>
        <Title level={2}>Composition Assignment</Title>
        <Title level={4}>{compositionTrim}</Title>
        <Title level={4}>Mono. Mass: {massMono}, Average Mass:{massAverage}</Title>
        <GlygenNote />

        


        <div> 
        <Row justify="start">
      <Col span={12} >
      <img object-fit="cover" height="220"  src={"https://swift.rc.nectar.org.au/v1/AUTH_c68cac76b84547d58ce4dc089f5f5844/compositions/" + composition + ".png" }  />           
     
      </Col>
      <Col span={12}>Publications</Col>
      
    
    </Row>
        </div>
        <Title level={4}>GlyGen-GlyTouCan Similar Entries</Title>
        <Table rowKey="toucan" columns={columns} dataSource={ data }  />

      </div>
    </Content>
    <FooterPage />
      </Layout>
       
       
 
    )
}


Composition.getInitialProps = async function ( context ) {

    const { query } = context;
    let composition = query.composition;
    console.log("check accession: " + composition )
    
    
    const res = await fetch('https://storage.googleapis.com/unicarbkb_compositions_toucan/composition_glytoucan.json');
    const json = await res.json();
    //const data = json.filter(t => t.composition ===  "comp_HexNAc3Hex4dHex1NeuAc1NeuGc0Pent0S0P0KDN0HexA0" ); //.filter(t => t.Position === '322^^xsd:int'); //.filter(t => t.Id.startsWith('comp'));
   

    const data = json.compositions_glytoucan.filter(t => t.composition ===  composition );
    
    let unique = new Set()
    let toucans = "GlyTouCan Entries ";
    let massMono 
    let massAverage
    for(let x of data){
        //console.log("yep: " + x.hexnac + " ---> " + JSON.stringify(x));
        unique.add(x.toucan)
        console.log("toucan: " + x.toucan + "\t" + x.mass_mom)
        massMono = parseFloat(x.mass_mom).toFixed(2)
        massAverage = parseFloat(x.mass_a).toFixed(2)
    }

    let compositionTrim = composition.toString().replace("comp_","").replace("HexA0", "").replace("NeuGc0","").replace("Pent0","").replace("S0","").replace("P0","").replace("KDN0","");


    //const comp = data.splice(1)
    //console.log("dick: " + comp.composition)
    
    //const uniprot =  accession.split("_",1)[0] 

    //console.log(accession)

    return { data, unique, toucans, composition, massMono, massAverage, compositionTrim}

}

export default Composition