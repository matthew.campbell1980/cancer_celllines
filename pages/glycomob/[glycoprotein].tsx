import fetch from 'isomorphic-unfetch';
import { Layout, Breadcrumb, Table, Typography, Tabs } from 'antd';
import HeaderPage from '../../components/HeaderPage';
import Link from 'next/link';
import GlygenNote from '../../components/GlygenNote';
import Comps from '../../components/Comps';
import Strs from '../../components/Strs';
import FooterPage from '../../components/FooterPage';
import Content from 'antd/lib/layout';
const { Title, Text } = Typography;
const { TabPane } = Tabs;

function callback(key) {
    console.log(key);
  }

const columns = [
        
    {
      title: 'CSS',   
      dataIndex: 'css',
      width: '20%',
      //align: "center",
  
    },
    {
      title: 'Mass',   
      dataIndex: 'mass',
      width: '20%',
    },
    {
      title: 'Hex',   
      dataIndex: 'hex',
      width: '20%',
      //align: "center",
    },
    {
      title: 'HexNAc',   
      dataIndex: 'hexnac',
      width: '20%',
      //align: "center",    
    },
    {
      title: 'dHex',   
      dataIndex: 'dhex',
      width: '20%',
      //align: "center",    
    },
    {
      title: 'NeuAc',   
      dataIndex: 'neunac',
      width: '20%',
      //align: "center",    
    }


  ]

function GlycomobStd({json, sodiated, gp, all}) {
    const hepositive = []
    const henegative = []

    const npositive = []
    const nnegative = []


    /*json.GP.filter(t => t.gas.match('He')).filter(t => t.mode.match('positive')).filter(t => t.mass > 0).map(x => hepositive.push( { css: x.css, mass: x.mass, hex: x.hex, hexnac: x.hexnac, dhex: x.dhex, neunac: x.neunac} )  )
    hepositive.sort(function (a, b) {
        return a.css - b.css;
    });

    console.log("he p: " + hepositive);

    json.GP.filter(t => t.gas.match('He')).filter(t => t.mode.match('negative')).filter(t => t.mass > 0).map(x => henegative.push( { css: x.css, mass: x.mass, hex: x.hex, hexnac: x.hexnac, dhex: x.dhex, neunac: x.neunac} )  )
    henegative.sort(function (a, b) {
        return a.css - b.css;
    });

    json.GP.filter(t => t.gas.match('N')).filter(t => t.mode.match('postive')).filter(t => t.mass > 0).map(x => npositive.push( { css: x.css, mass: x.mass, hex: x.hex, hexnac: x.hexnac, dhex: x.dhex, neunac: x.neunac} )  )
    npositive.sort(function (a, b) {
        return a.css - b.css;
    });

    json.GP.filter(t => t.gas.match('N')).filter(t => t.mode.match('negative')).filter(t => t.mass > 0).map(x => nnegative.push( { css: x.css, mass: x.mass, hex: x.hex, hexnac: x.hexnac, dhex: x.dhex, neunac: x.neunac} )  )
    nnegative.sort(function (a, b) {
        return a.css - b.css;
    });*/

    all.glycomGlycoproteins.filter(t => t.gas.match('He')).filter(t => t.glycoprotein.imatch({gp})).filter(t => t.mode.imatch('positive')).filter(t => t.mass > 0).map(x => hepositive.push( { css: x.css, mass: x.mass, hex: x.hex, hexnac: x.hexnac, dhex: x.dhex, neunac: x.neunac} )  )
    hepositive.sort(function (a, b) {
        return a.css - b.css;
    });

    all.glycomGlycoproteins.filter(t => t.gas.match('He')).filter(t => t.glycoprotein.imatch({gp})).filter(t => t.mode.imatch('negative')).filter(t => t.mass > 0).map(x => henegative.push( { css: x.css, mass: x.mass, hex: x.hex, hexnac: x.hexnac, dhex: x.dhex, neunac: x.neunac} )  )
    henegative.sort(function (a, b) {
        return a.css - b.css;
    });

    all.glycomGlycoproteins.filter(t => t.gas.match('N')).filter(t => t.glycoprotein.imatch({gp})).filter(t => t.mode.imatch('postive')).filter(t => t.mass > 0).map(x => npositive.push( { css: x.css, mass: x.mass, hex: x.hex, hexnac: x.hexnac, dhex: x.dhex, neunac: x.neunac} )  )
    npositive.sort(function (a, b) {
        return a.css - b.css;
    });


    all.glycomGlycoproteins.filter(t => t.gas.match('N')).filter(t => t.glycoprotein.imatch({gp})).filter(t => t.mode.imatch('Negative')).filter(t => t.mass > 0).map(x => nnegative.push( { css: x.css, mass: x.mass, hex: x.hex, hexnac: x.hexnac, dhex: x.dhex, neunac: x.neunac} )  )
    nnegative.sort(function (a, b) {
        return a.css - b.css;
    });


    /*sodiated.GP.filter(t => t.gas.match('N')).filter(t => t.mode.match('postive')).map(x => npositive.push( { css: x.css, mass: x.mass, hex: x.hex, hexnac: x.hexnac, dhex: x.dhex, neunac: x.neunac} )  )
    npositive.sort(function (a, b) {
        return a.css - b.css;
    });

    sodiated.GP.filter(t => t.gas.match('N')).filter(t => t.mode.match('postive')).map(x => npositive.push( { css: x.css, mass: x.mass, hex: x.hex, hexnac: x.hexnac, dhex: x.dhex, neunac: x.neunac} )  )
    npositive.sort(function (a, b) {
        return a.css - b.css;
    });*/


    return (
        <Layout className="layout">
         <HeaderPage />
         <Content style={{ padding: '0 50px', marginTop: 20 }}>
        <Breadcrumb style={{ margin: '24px 0' }}>
          <Breadcrumb.Item>Home</Breadcrumb.Item>
          <Breadcrumb.Item>GlycoMob</Breadcrumb.Item>

        </Breadcrumb>
        <div style={{ background: '#fff', padding: 24 }}>
        <Title level={2}>Glycoprotein CCS Values</Title>

        <Text>
        Ion mobility mass spectrometry (IM-MS) separates glycan ions based on their collision cross section (CCS) and provides glycan precursor and fragment masses. It has been shown that isomeric oligosaccharide species can be separated by IM and identified on basis of their CCS and fragmentation. These results indicate that adding CCSs information for glycans and glycan fragments to searchable databases and analysis pipelines will increase identification confidence and accuracy. GlycoMob contains over 900 CCSs values of glycans, oligosaccharide standards and their fragments. We have measured the absolute CCSs of calibration standards, biologically derived and synthetic N-glycans ionized with various adducts in positive and negative mode or as protonated (positive ion) and deprotonated (negative ion) ions.    </Text>
        <Tabs  onChange={callback}>
            <TabPane tab="Helium Positive" key="heliumpos">
                <Table rowKey="css" columns={columns} dataSource={ hepositive }  />
            </TabPane>

            <TabPane tab="Helium Negative" key="heliumneg">
                <Table rowKey="css" columns={columns} dataSource={ henegative }  />
            </TabPane>

            <TabPane tab="Nitrogen Positive" key="nitrogenpos">
                <Table rowKey="css" columns={columns} dataSource={ npositive }  />
            </TabPane>

            <TabPane tab="Nitrogen Negative" key="nitrogenneg">
                <Table rowKey="css" columns={columns} dataSource={ nnegative }  />
            </TabPane>
            
            
        </Tabs>
        
        </div>
      </Content>
      <FooterPage />
        </Layout>
      )
  }

GlycomobStd.getInitialProps = async function ( context ) {
  
  
  
    const { query } = context;
    const gp = query.glycoprotein;
    console.log("check accession: " + gp )

    //const res = await fetch('https://storage.googleapis.com/glycomob/' + query.glycoprotein + '.json');
    const res = await fetch('https://storage.googleapis.com/glycomob/Glycoproteins/adduct/non_sodiated/fetuinds.json'); //' + query.glycoprotein + '.json');
    const json = await res.json();
  
    const res2 = await fetch('https://storage.googleapis.com/glycomob/Glycoproteins/adduct/sodiated/' + query.glycoprotein + '_sodiated.json');
    const sodiated = await res2.json();

    const res3 = await fetch('https://storage.googleapis.com/glycomob/Glycoproteins/test/test2.json');
    const all = await res3.json();
    
    //const proteinName = json.Dextran.filter(t => t.gas.match('He')  ) ; //' //'.map( t =>  t.fullName    );
    //const sodiated = json2.Dextran.filter(t => t.gas.match('He')  ) ;
  
    /*const strProtein = await fetch('https://storage.googleapis.com/unicarbkb_glycoproteins/structure_proteins/' + query.accession + '.json');
    const strProteins = await strProtein.json();
    const comps = [] = strProteins.comp;
    const toucans = [] = strProteins.glytoucan;
    const strs = [] = strProteins.structureId;
    let compsCount = comps.length;
    let strCount = strs.length;*/
    //json.glycosites.map(x => data2.push( { accession: json.accession, site: x.site ,  pmid: x.pmids , glycans: x.glycanStructures} ))
  
    console.log("json: " + sodiated)
    
    return {  json , sodiated, gp, all };
  };
  
  export default GlycomobStd;