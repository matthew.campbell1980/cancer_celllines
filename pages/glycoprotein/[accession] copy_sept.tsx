import { Table, Layout, Breadcrumb, Tag, Tabs, Button, Modal } from 'antd';
import Content from 'antd/lib/layout';
import { CSSProperties, Fragment } from 'react'
import { Typography } from 'antd';
import fetch from 'isomorphic-unfetch';
import HeaderPage from '../../components/HeaderPage';
import FooterPage from '../../components/FooterPage';
import GlygenNote from '../../components/GlygenNote';
import Comps from '../../components/Comps';
import Strs from '../../components/Strs';
import Link from 'next/link';
import _ from 'lodash';
import CovidSpecial from '../../components/CovidSpecial';
import CurationNotes from '../../components/CurationNotes';
import Instrumentation from '../../components/Instrumentation';
import Fragmentation from '../../components/Fragmentation';
import Glycopeptidedigest from '../../components/Glycopeptidedigest';
import React from 'react';
//import ToggleBox from '../ToggleBox';
import MyLink from '../MyLink';
//  import Iframe from 'react-iframe'
const { Title, Text } = Typography;
const { TabPane } = Tabs;



const demos = {
  soundcloud:
    '<iframe width="90% seamless="seamless"" height="525" frameborder="0" scrolling="no" src="//plotly.com/~mpcamp/1.embed"></iframe>',
  
    site17:
    '<iframe width="auto" height="auto" frameborder="0" scrolling="no" src="//plotly.com/~mpcamp/4.embed"></iframe>',

    site61:
    '<iframe width="900" height="800" frameborder="0" scrolling="no" src="//plotly.com/~mpcamp/11.embed"></iframe>',
    site74:
    '<iframe width="900" height="800" frameborder="0" scrolling="no" src="//plotly.com/~mpcamp/13.embed"></iframe>',

    site122:
    '<iframe width="900" height="800" frameborder="0" scrolling="no" src="//plotly.com/~mpcamp/18.embed"></iframe>',

    site149:
    '<iframe width="900" height="800" frameborder="0" scrolling="no" src="//plotly.com/~mpcamp/20.embed"></iframe>',

    site165:
    '<iframe width="900" height="800" frameborder="0" scrolling="no" src="//plotly.com/~mpcamp/22.embed"></iframe>',

    site234:
    '<iframe width="900" height="800" frameborder="0" scrolling="no" src="//plotly.com/~mpcamp/24.embed"></iframe>',

    site282:
    '<iframe width="900" height="800" frameborder="0" scrolling="no" src="//plotly.com/~mpcamp/26.embed"></iframe>',

    site331:
    '<iframe width="900" height="800" frameborder="0" scrolling="no" src="//plotly.com/~mpcamp/28.embed"></iframe>',

    site343:
    '<iframe width="900" height="800" frameborder="0" scrolling="no" src="//plotly.com/~mpcamp/30.embed"></iframe>',

    site603:
    '<iframe width="900" height="800" frameborder="0" scrolling="no" src="//plotly.com/~mpcamp/32.embed"></iframe>',

    site616:
    '<iframe width="900" height="800" frameborder="0" scrolling="no" src="//plotly.com/~mpcamp/34.embed"></iframe>',

    site657:
    '<iframe width="900" height="800" frameborder="0" scrolling="no" src="//plotly.com/~mpcamp/36.embed"></iframe>',

    site709:
    '<iframe width="900" height="800" frameborder="0" scrolling="no" src="//plotly.com/~mpcamp/38.embed"></iframe>',

    site717:
    '<iframe width="900" height="800" frameborder="0" scrolling="no" src="//plotly.com/~mpcamp/40.embed"></iframe>',

    site801:
    '<iframe width="900" height="800" frameborder="0" scrolling="no" src="//plotly.com/~mpcamp/42.embed"></iframe>',

    site1074:
    '<iframe width="900" height="800" frameborder="0" scrolling="no" src="//plotly.com/~mpcamp/44.embed"></iframe>',

    site1098:
    '<iframe width="900" height="800" frameborder="0" scrolling="no" src="//plotly.com/~mpcamp/46.embed"></iframe>',

    site1134:
    '<iframe width="900" height="800" frameborder="0" scrolling="no" src="//plotly.com/~mpcamp/48.embed"></iframe>',

    site1158:
    '<iframe width="900" height="800" frameborder="0" scrolling="no" src="//plotly.com/~mpcamp/50.embed"></iframe>',

    site1173:
    '<iframe width="900" height="800" frameborder="0" scrolling="no" src="//plotly.com/~mpcamp/52.embed"></iframe>',

    site1194:
    '<iframe width="900" height="800" frameborder="0" scrolling="no" src="//plotly.com/~mpcamp/54.embed"></iframe>',
};

//codesandbox.io/s/react-iframe-demo-g3vst codePen =
function Iframe(props) {
  return (
    <div
      dangerouslySetInnerHTML={{ __html: props.iframe ? props.iframe : "" }}
    />
  );
}




const myStyles: CSSProperties = {
  width: 300,
  height: 200,
  resizeMode: 'contain',
} as React.CSSProperties;

const columns = [
        
  /*{
    title: 'UniProt Accession',   
    dataIndex: 'accession',
    width: '20%',
    //align: "center",

  },*/
  {
    title: 'Site',   
    dataIndex: 'site',
    width: '10%',
  },
  {
    title: 'Glycosylation Type',   
    dataIndex: 'glycanType',
    width: '20%',
    //align: "center",
  },
  {
    title: 'GlyTouCan Id',   
    dataIndex: 'comptoucan',
    width: '10%',
    //align: "center",
  },
  /*{
    title: 'Glycan Entries',   
    dataIndex: 'glycans',
    width: '10%',
    //align: "center",
  },*/
  {
    title: 'Comp. Image',   
    dataIndex: 'comp',
    width: '20%',
    //align: "center",
    //render: (text, record) => <img src={"https://swift.rc.nectar.org.au/v1/AUTH_c68cac76b84547d58ce4dc089f5f5844/compositions/" + text + ".png" } style={myStyles} />  
  render: (text, record) => <img src={"https://storage.googleapis.com/unicarbkb_comps_imgs/" + text + ".png"}  alt='' />
  },
  {
    title: 'Publications',   
    dataIndex: 'pmid',
    width: '10%',
    tags: 'pmid',
    //align: "center", 
    render: pmid =>   pmid.map( x => { return <Tag color="#3ECF8E"><a href='https://www.ncbi.nlm.nih.gov/pubmed/${x}'>PMID: {x}</a></Tag> } )
  },
  {
    title: 'Preprint',   
    dataIndex: 'doi',
    width: '20%',
    render: doi =>   doi.map( x => { return <Tag color="#6772e5"><Link href='${x}' as={`${x}`}><a>{x}</a></Link></Tag> } )
  },
  {
    title: 'Abundance',   
    dataIndex: 'abundance',
    width: '20%',
  }
    
]


function callback(key) {
  console.log(key);
}


//function GlycoproteinEntry({ json, accession, proteinName, comps, toucans, strs, compsCount, strCount, species, pubPmid, pubSummaryjson }) {
function GlycoproteinEntry({ json, accession, proteinName,  strCount, species, pubPmid, pubSummaryjson, instrumentationUnique, gpenzymes, fragmentation, expression, additionalNotes, data2, instrumentFinal, gpenzymeFinal, fragFinal, expressionFinal, additionalNotesFinal }) {

    
  //const urlReg =  new RegExp("^(http[s]?:\\/\\/(www\\.)?|ftp:\\/\\/(www\\.)?|www\\.){1}([0-9A-Za-z-\\.@:%_\+~#=]+)+((\\.[a-zA-Z]{2,3})+)(/(.)*)?(\\?(.)*)?");
  const urlReg = /(http.*),/gi; 
 

  let p = []
  

  data2.sort(function (a, b) {
    return a.site - b.site;
  });

  //console.log("TESTING: " + JSON.stringify(data2)  )
  const x2 = [];
  json.glycosites.map(x => x2.push( x[2]  )  )

  const compId =  [] = json.glycosites.map(x => x.comp );
  
  const compIdUnique = compId.filter((value, index, array) => 
     !array.filter((v, i) => JSON.stringify(value) == JSON.stringify(v) && i < index).length);

  //console.log("enz" + expression);

  /*363 
  abby 
  53663
 */


  let covid: {}
  if (accession == "P0DTC2"){
    covid = <CovidSpecial />
  }

 
  //const g = _.groupBy(json, "site");


    return (
      <Layout>
       <HeaderPage />
       <Content style={{ padding: '0 50px', marginTop: 20 }}>
      <Breadcrumb style={{ margin: '24px 0' }}>
        <Breadcrumb.Item>Home</Breadcrumb.Item>
        <Breadcrumb.Item>Glycoprotein</Breadcrumb.Item>
        <Breadcrumb.Item>{accession}</Breadcrumb.Item>
      </Breadcrumb>
      <div style={{ background: '#fff', padding: 24 }}>
        <Title level={2}>{proteinName}</Title>
        <Title level={3}><a href='https://www.uniprot.org/uniprot/${accession}'>UniProt:  {accession}</a></Title>
        <Title level={3}>Species: {species}</Title>
      
        
        <GlygenNote />
        {covid}

      

        <Comps style="max-width: 30px" compsCount={compIdUnique.length} /><br />
        <Strs strCount={strCount} />

        <Tabs defaultActiveKey="1" onChange={callback} animated={false}>
        <TabPane tab="Data Summary" key="1">
          <Table columns={columns} dataSource={ data2 }  />
        </TabPane>
        <TabPane tab="Experimental Notes" key="4">
          <div>
          <Instrumentation instrument={instrumentFinal} />
          <Glycopeptidedigest gpenzymes={gpenzymeFinal} />
          <Fragmentation fragmentation={fragFinal} />
          <CurationNotes expression={expressionFinal} />
          </div>
          </TabPane>
        <TabPane tab="Data Visulisation" key="3">
          <a href="22">test</a>
        
          



        
        <MyLink  />

      </TabPane>
      </Tabs>
      </div>
    </Content>
    <FooterPage />
      </Layout>
    )
}

GlycoproteinEntry.getInitialProps = async function ( context ) {


  
  let pubPmid = []
  try{
    pubPmid = context.req.url.split('?')[1].split('=')[1];
  }catch(err){
  }
  console.log("testingL " + pubPmid )

  
  const { query } = context;
  const accession = query.accession;
 
  const res = await fetch('https://storage.googleapis.com/unicarbkb_glycoproteins/summary/' + query.accession + '.json');
  const json = await res.json();

  const pubSummary = await fetch('https://storage.googleapis.com/unicarbkb_glycoproteins/protein_pub_str/pub_' + query.accession + '.json');
  const pubSummaryjson = await pubSummary.json();
  //const pubSummaryjson = await file.json();

  const p = await fetch('https://storage.googleapis.com/unicarbkb_glycoproteins/allGlycoproteins.json');
  const proteins = await p.json();
  const species = json.species
  const proteinName = proteins.glycoproteins.glycoprotein.filter(t => t.accession ===  accession  ).map( t =>  t.fullName    );

  /*const strProtein = await fetch('https://storage.googleapis.com/unicarbkb_glycoproteins/structure_proteins/' + query.accession + '.json');
  const strProteins = await strProtein.json();
  const comps = [] = strProteins.comp;
  const toucans = [] = strProteins.glytoucan;
  const strs = [] = strProteins.structureId;
  let compsCount = comps.length;
  let strCount = strs.length;*/

  let data2 = [];
  let instrumentation = [];
  let expression = [];
  let gpenzymes = [];
  let instrument = [];
  //let instrumentationUnique = [];
  let fragmentation = [];
  let additionalNotes = [];
  let instrumentationUnique = new Set()
  let gpenzymesUnique = new Set()
  let fragmentationUnique = new Set()
  let expressionUnique = new Set()
  let additionalNotesUnique = new Set()
  let instrumentFinal = ""
  let gpenzymeFinal = ""
  let fragFinal = ""
  let expressionFinal = ""
  let additionalNotesFinal = ""


  if(pubPmid.length == 0){
    json.glycosites.map(x => data2.push( { accession: accession, glycanType: x.glycanType , doi2: x.doi2 , site: x.site , composition: x.comp.toString().trim() , pmid: x.pmid , glycans: x.glycanStructures , comp: x.comp , doi: x.doi , comptoucan: x.comp_toucan, glycoType: x.glycoType, abundance: parseFloat(x.abundance).toFixed(2) } )  )
  //.map(x2 => data2.push( {doi2: x2.doi2 }) )
  } else {
    let search = pubPmid
    pubSummaryjson.glycosites.filter(t => t.pmid == search)
    .map(x => data2.push( { additionalNotes: x.additionalNotes , expression_purification: x.expression_purification , fragmentation: x.fragmentation , instrumentation: x.instrumentation , expression: x.expression_purification , gpenzymes: x.gp_enzymes , accession: accession, glycanType: x.glycanType , doi2: x.doi2 , site: x.site , composition: x.comp.toString().trim() , pmid: x.pmid , glycans: x.glycanStructures , comp: x.comp , doi: x.doi , comptoucan: x.comp_toucan, glycoType: x.glycoType, abundance: parseFloat(x.abundance).toFixed(2) } )  )
    
     const instrument = [] = data2.map(x => x.instrumentation) //push( { instrumentation: x.instrumentation } ) )
     
     instrumentationUnique = new Set(data2.map(x => x.instrumentation));
     var i = instrumentationUnique.values()
     instrumentFinal = i.next().value 

     //const gpenzymes = [] = pubSummaryjson.glycosites.filter(t => t.pmid == search).map(x => x.gp_enzymes)
     gpenzymesUnique = new Set(data2.map(x => x.gpenzymes));
     var i = gpenzymesUnique.values()
     gpenzymeFinal = i.next().value 
 
     //const fragmentation = [] = pubSummaryjson.glycosites.filter(t => t.pmid == search).map(x => x.fragmentation)
     fragmentationUnique = new Set(data2.map(x => x.fragmentation));
     var i = fragmentationUnique.values()
     fragFinal = i.next().value 

     //const expression = [] = pubSummaryjson.glycosites.filter(t => t.pmid == search).map(x => x.expression_purification)
     expressionUnique = new Set(data2.map(x => x.expression_purification));
     var i = expressionUnique.values()
     expressionFinal = i.next().value 
 
     //const additionalNotes = [] = pubSummaryjson.glycosites.filter(t => t.pmid == search).map(x => x.additionalNotes)
     additionalNotesUnique = new Set(data2.map(x => x.additionalNotes));
     var i = additionalNotesUnique.values()
     additionalNotesFinal = i.next().value 



  }

  
  //return {  json, accession , proteinName, comps, toucans, strs, compsCount, strCount, species, pubPmid, pubSummaryjson};
  return {  json, accession , proteinName,  species, pubPmid, pubSummaryjson, instrumentationUnique, gpenzymes, fragmentation, expression, additionalNotes, data2, instrumentFinal, gpenzymeFinal, fragFinal, expressionFinal, additionalNotesFinal};
};

export default GlycoproteinEntry;