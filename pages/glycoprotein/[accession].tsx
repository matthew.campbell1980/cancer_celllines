import { Table, Layout, Breadcrumb, Tag, Tabs, List, Badge } from 'antd';
import Content from 'antd/lib/layout';
import { Typography } from 'antd';
import fetch from 'isomorphic-unfetch';
import HeaderPage from '../../components/HeaderPage';
import FooterPage from '../../components/FooterPage';
import GlygenNote from '../../components/GlygenNote';

import { BookOutlined} from '@ant-design/icons';
//import NextLink from '@moxy/next-link';

import Link from 'next/link';
import _ from 'lodash';

import React from 'react';

//  import Iframe from 'react-iframe'
const { Title, Text } = Typography;
const { TabPane } = Tabs;

interface publicatonSummary {
  title?: string;
  pubdate?: string;
  lastauthor?: string;
  fulljournalname?: string;
  pmid?: BigInteger;

}



const columns = [
  {
    title: 'Image',
    dataIndex: 'toucan',
    key: 'image',
    width: '40%',
    fixed: true,
    render: (text) => //<Image width={200} src="https://zos.alipayobjects.com/rmsportal/jkjgkEfvpUPVyRjUImniVslZfWPnJuuZ.png"/>
  <img style={{ flex:0 }}  width={300} src={"https://api.glygen.org/glycan/image/" + text } /> 
  },
  {
    title: 'Site',   
    dataIndex: 'site',
    width: '10%',
    sorter: {
      compare: (a, b) => a.site - b.site,
      multiple: 1,
    },
  },
  /*{
    title: 'Glycosylation Type',   
    dataIndex: 'glycanType',
    width: '20%',
    //align: "center",
  },*/
  {
    title: 'Amino Acid',   
    dataIndex: 'TypeAminoAcid',
    width: '20%',
    //align: "center",
  },
  {
    title: 'GlyTouCan Id',   
    dataIndex: 'toucan',
    width: '10%',
    //align: "center",
  } 
]

const globalcolumns = [
  {
    title: 'Image',
    dataIndex: 'toucan',
    key: 'image',
    width: '40%',
    fixed: true,
    render: (text) => //<Image width={200} src="https://zos.alipayobjects.com/rmsportal/jkjgkEfvpUPVyRjUImniVslZfWPnJuuZ.png"/>
  <img style={{ flex:1 }}  height={300} src={"https://api.glygen.org/glycan/image/" + text } /> 
  },
  /*{
    title: 'Glycosylation Type',   
    dataIndex: 'glycanType',
    width: '20%',
    //align: "center",
  },*/
  {
    title: 'Amino Acid',   
    dataIndex: 'TypeAminoAcid',
    width: '20%',
    //align: "center",
  },
  {
    title: 'GlyTouCan Id',   
    dataIndex: 'toucan',
    width: '10%',
    //align: "center",
  } 
]


function callback(key) {
  console.log(key);
}

let testing:any = {
  title: 'shshhs',
  pubdate: '23',
  lastauthor: 'me',
  fulljournalname: 'bored'
}


//function GlycoproteinEntry({ json, accession, proteinName, comps, toucans, strs, compsCount, strCount, species, pubPmid, pubSummaryjson }) {
function GlycoproteinEntry({ siteSpecific, global, accession, sitesUnique, sitesNUnique, sitesOUnique, pubDetails }) {



    return (
      <Layout>
       <HeaderPage />
       <Content style={{ padding: '0 50px', marginTop: 20 }}>
      <Breadcrumb style={{ margin: '24px 0' }}>
        <Breadcrumb.Item>Home</Breadcrumb.Item>
        <Breadcrumb.Item>Glycoprotein</Breadcrumb.Item>
        <Breadcrumb.Item>{accession}</Breadcrumb.Item>
      </Breadcrumb>
      <div style={{ background: '#fff', padding: 24 }}>
      <GlygenNote/>
        <Title level={3}><a href='https://www.uniprot.org/uniprot/${accession}'>UniProt: {accession} </a></Title>
        <Tag  color="#3b5999">Curated Publications</Tag>
        <List itemLayout="horizontal" dataSource={pubDetails} size="small" renderItem={item => (
        <List.Item>
        {item['title']}, {item['lastauthor']}, {item['fulljournalname']}, {item['pubdate']}<Link href={`/publication/${item['pmid']}`}><a><Tag color="success">View</Tag></a></Link>
        </List.Item>
        )} />

  
        <Tabs defaultActiveKey="1" onChange={callback} animated={false}>
          <TabPane tab="Site-Specific" key="1">
            <Title level={4}>Sites {sitesUnique.length}</Title>
            <Text>N-Linked <Badge  count={sitesNUnique.length} style={{ backgroundColor: '#87d068' }}/></Text>
            <Text>O-Linked <Badge  count={sitesOUnique.length} style={{ backgroundColor: '#87d068' }}/></Text>
            <Table columns={columns} dataSource={ siteSpecific }  />
          </TabPane>
          <TabPane tab="Global" key="2">
            <Table columns={globalcolumns} dataSource={ global }  />
          </TabPane>
        </Tabs>
    
        
  
      </div>
    </Content>
    <FooterPage />
      </Layout>
    )
}

GlycoproteinEntry.getInitialProps = async function ( context ) {

  const { query } = context;
  const accession = query.accession;
 const json = []; 
 let pubDetails: publicatonSummary[] = []
  /*const res = await fetch('https://storage.googleapis.com/humantemp/human.json');
  
  const json = await res.json();

  const rat = await fetch('https://storage.googleapis.com/humantemp/rat.json');
  const jsonrat = await rat.json();

  const mouse = await fetch('https://storage.googleapis.com/humantemp/mouse.json');
  const jsonmouse = await mouse.json();
  */

  
  //const resm = await fetch('https://storage.googleapis.com/humantemp/allmerged231020.json');
  const resm = await fetch('https://storage.googleapis.com/humantemp/allmerged060321.json');

  const merged = await resm.json();

  const siteSpecific = []
  const global = []

  merged.human.filter(t => t.uniprot ===  accession ).filter(t => t.position != null).map(x => siteSpecific.push( { uniprot: x.uniprot, pmid: x.pmid, site: x.position, TypeAminoAcid: x.TypeAminoAcid, toucan: x.toucan } )  )
  merged.human.filter(t => t.uniprot ===  accession ).filter(t => t.position === "" ).map(x => global.push( { uniprot: x.uniprot, pmid: x.pmid, TypeAminoAcid: x.TypeAminoAcid, toucan: x.toucan } )  )
  
  let sites = []
  sites = merged.human.filter(t => t.uniprot ===  accession ).filter(t => t.position != null).map( t =>  t.position);
  let nsites = merged.human.filter(t => t.uniprot ===  accession ).filter(t => t.GlycanType == "N-linked").map( t =>  t.position);
  let osites = merged.human.filter(t => t.uniprot ===  accession ).filter(t => t.GlycanType === "O-linked").map( t =>  t.position);

  const nSiteStr = [] 
  merged.human.filter(t => t.uniprot ===  accession ).filter(t => t.GlycanType == "N-linked").map( t => nSiteStr.push({ position: t.position }))

  const oSiteStr = []
  merged.human.filter(t => t.uniprot ===  accession ).filter(t => t.GlycanType == "O-linked").map( t => nSiteStr.push({ position: t.position }))


  const pmids = merged.human.filter(t => t.uniprot ===  accession  ).map( t =>  t.pmid);



  const uniqueSet = new Set(sites);
  let sitesUnique = Array.from(new Set(uniqueSet));

  const uniqueNSites = new Set(nsites);
  let sitesNUnique = Array.from(new Set(uniqueNSites));

  const uniqueOSites = new Set(osites);
  let sitesOUnique = Array.from(new Set(uniqueOSites));

  const uniqueNStrs = new Set(nSiteStr);
  let strsNUnique = Array.from(new Set(uniqueNStrs));

  const uniqueOStrs = new Set(oSiteStr);

let unique = nSiteStr.filter((item, i, ar) => ar.indexOf(item) === i);
let uniqueO = oSiteStr.filter((item, i, ar) => ar.indexOf(item) === i);
//console.log(unique);

var arr2 = unique.reduce( (a,b) => {
  var i = a.findIndex( x => x.position === b.position);
  return i === -1 ? a.push({ position : b.position, times : 1 }) : a[i].times++, a;
}, []);

var arr3 = uniqueO.reduce( (a,b) => {
  var i = a.findIndex( x => x.position === b.position);
  return i === -1 ? a.push({ position : b.position, times : 1 }) : a[i].times++, a;
}, []);

  const pmidsIdUnique = pmids.filter((value, index, array) => 
     !array.filter((v, i) => JSON.stringify(value) == JSON.stringify(v) && i < index).length);

  let publicatonDetails: Array<publicatonSummary>

  for (var pmid of pmidsIdUnique) {
    const pub = await fetch('https://eutils.ncbi.nlm.nih.gov/entrez/eutils/esummary.fcgi?db=pubmed&id=' + pmid + '&retmode=json&tool=my_tool&email=my_email@example.com');
    const bioc = await pub.json();
    pubDetails.push( { pmid: pmid, pubdate: bioc.result[pmid].pubdate , lastauthor: bioc.result[pmid].lastauthor, fulljournalname: bioc.result[pmid].fulljournalname, title: bioc.result[pmid].title })
 
  }

  return { json, pmidsIdUnique, siteSpecific, global, accession, sitesUnique, sitesNUnique, sitesOUnique, strsNUnique, arr2, arr3, pubDetails };

}



export default GlycoproteinEntry;
