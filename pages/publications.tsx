import React, { useState, useEffect, Fragment, forwardRef } from 'react';
import { useRouter } from 'next/router'
import fetch from 'isomorphic-unfetch';
import { Table, Row, Col, Tag,  PageHeader, Tabs, Button, Statistic, Descriptions, Avatar, Tooltip, AutoComplete, Menu, Breadcrumb, Divider, List, Badge, Typography } from 'antd';

const { Footer } = Layout;
import Layout from 'antd/lib/layout';

import Content from 'antd/lib/layout';

import Header from 'antd/lib/calendar/Header';
import FooterPage from '../components/FooterPage';
import PublicationsNote from '../components/PublicationsNote';
import Link from 'next/link';
import HeaderPage from '../components/HeaderPage';
const { Title, Text } = Typography;


interface publicatonSummary {
  title?: string;
  pubdate?: string;
  lastauthor?: string;
  fulljournalname?: string;
  pmid?: BigInteger;

}

const columns = [
  {
    title: 'Title',
    dataIndex: 'title',
    key: 'title',
    width: '50%',
    fixed: true,
   // ...this.getColumnSearchProps('title'),
  },
  {
    title: 'Last Author',   
    dataIndex: 'lastauthor',
    width: '10%',
  },
  {
    title: 'Year',
    dataIndex: 'pubdate',
    width: '10%',
  },
  {
    title: 'Journal',   
    dataIndex: 'fulljournalname',
    width: '20%'
  },
  {
    title: 'hello', 
    width: '10%',
    render: (text) => <Link href={`/publication/`}><a><Tag color="success">View</Tag></a></Link>
  }
]

const publicationsData = [
  {
    title: 'title',
    pmid: 'pmid',
    gs: 'gs',
  }

];



const data = [];

function getQuery(){
  const router = useRouter()
  const { id } = router.query
  console.log("id check " + id)
}

function callback(key) {
  console.log(key);
}

const tData = [];

const ref = React.createRef();

function Publications({ json, pubDetails  }) {
  
    //return <div>Next stars: {stars} and test: {test} 
    //<div>works</div> </div>    
    console.log("testing json " +  json.cellPublications.items.map( t10 => t10.pmid ) ) ;
    console.log("testing json " + json.cellPublications.items.map( t10 => publicationsData.push( { title: t10.title , pmid: t10.pmid, gs: t10.gs} ) ) );
    //jsonTCP13.melanoma.cart_items.map( t10 => tcp13Data.push( { name: t10.name  , atcc: t10.atcc , cellosaurus: t10.cellosaurus } ) ); 
  

    return (
      <Layout className="layout">
        <HeaderPage />
   
    <Content style={{ padding: '0 50px' }}>
      <Breadcrumb style={{ margin: '16px 0' }}>
        <Breadcrumb.Item>UniCarbKB-GlyGen</Breadcrumb.Item>
        <Breadcrumb.Item>Publications</Breadcrumb.Item>
      </Breadcrumb>
      <div style={{ background: '#fff', padding: 20, minHeight: 280 }}> 
      <PublicationsNote/>
      <Text>Total Publications: <Badge overflowCount={2000}  count={pubDetails.length} style={{ backgroundColor: '#87d068' }}/></Text>
      <Table columns={columns} dataSource={ pubDetails }  />
      </div>
    </Content>
    <FooterPage/>
  </Layout>

  


    )
  }
  
  Publications.getInitialProps = async ({ req }) => {
    const publications = []
    let pubDetails: publicatonSummary[] = []
    //const res = await fetch('https://api.github.com/repos/zeit/next.js');
    
    //const res = await fetch('https://storage.googleapis.com/cellosaurus/CVCL_0026.json');
    const res = await fetch('https://storage.googleapis.com/journal-image/test3.json'); //CVCL_0026.json');
    const json = await res.json();

    /*const resm = await fetch('https://storage.googleapis.com/humantemp/allmerged151020.json');
    const data = await resm.json();

    data.human.filter(t => t.pmid > 0  ).map(x => publications.push( { pmid: x.pmid } )  )
    const uniquePmids = new Set(publications);
    let pmidsIdUnique = Array.from(new Set(uniquePmids));

    var arr3 = pmidsIdUnique.reduce( (a,b) => {
      var i = a.findIndex( x => x.pmid === b.pmid);
      return i === -1 ? a.push({ pmid : b.pmid, times : 1 }) : a[i].times++, a;
    }, []);

    for (var pmid of arr3) {
      const pub = await fetch('https://eutils.ncbi.nlm.nih.gov/entrez/eutils/esummary.fcgi?db=pubmed&id=' + pmid.pmid + '&retmode=json&tool=my_tool&email=my_email@example.com');
      const bioc = await pub.json();

      console.log("pmid: " + pmid.pmid)
      let p = pmid.pmid
      
      pubDetails.push( { pmid: pmid, pubdate: bioc.result[p].pubdate , lastauthor: bioc.result[p].lastauthor, fulljournalname: bioc.result[p].fulljournalname, title: bioc.result[p].title })
     
    } */

    const resm = await fetch('https://storage.googleapis.com/humantemp/site_pubsa.json');
    const data = await resm.json();
    console.log("pmid c " + data)
    data.publications.map(x => pubDetails.push( { pmid: x.pmid, pubdate: x.pubdate , lastauthor: x.lastauthor, fulljournalname: x.fulljournalname, title: x.title }) )
    

    console.log("out " + JSON.stringify(pubDetails) )

    pubDetails.sort((a, b) => (b.pubdate > a.pubdate) ? 1 : -1)
    
    return { json, pubDetails};
  };


  
  export default Publications;


  /*
      <List itemLayout="horizontal" dataSource={publicationsData} renderItem={item => (
        <List.Item>
        <List.Item.Meta
          avatar={<Avatar src="https://storage.googleapis.com/journal-image/mcp.png" />}
          title={<a href="https://www.ncbi.nlm.nih.gov/pubmed/">{item.title} {item.pmid} </a>}
          description="Ant Design, a design language for background applications, is refined by Ant UED Team"
        /><a href="">hello</a>
        </List.Item>
      )} />

      <div><Avatar size="large" src="https://storage.googleapis.com/journal-image/mcp.png" /><p>testing this sentence ofr now to add</p></div>
      </div>
      */

      /* <Descriptions title="" layout="horizontal" column={1}>
        <Descriptions.Item label="Project">Glycome profiling of established {json.ID} Cell Lines</Descriptions.Item>
        <Descriptions.Item label="Subjects">
          <Tag color="orange"><Tooltip title="Ontology: http://purl.obolibrary.org/obo/NCIT_C44282"><span>{json.DI}</span></Tooltip></Tag> 
          <Tag color="orange"><Tooltip title="Ontology: http://purl.obolibrary.org/obo/NCIT_C44282"><span>{json.OX}</span></Tooltip></Tag> 
        </Descriptions.Item>
        <Descriptions.Item label="Subjects">
          <Tag color="blue"><Tooltip title="Ontology: http://purl.obolibrary.org/obo/NCIT_C18816"><span>{json.SX}</span></Tooltip></Tag> 
          <Tag color="blue"><Tooltip title="Ontology: http://purl.obolibrary.org/obo/NCIT_C18816"><span>{json.AG}</span></Tooltip></Tag> 
        </Descriptions.Item>
      </Descriptions>
      <Divider orientation="left">Publications</Divider>*/
  