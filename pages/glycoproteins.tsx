import { Table, Input, Button, Layout, Breadcrumb } from 'antd';
import { SearchOutlined } from '@ant-design/icons';
import Highlighter from 'react-highlight-words';
import React, { Component } from 'react';
import fetch from 'isomorphic-unfetch';
import Content from 'antd/lib/layout';
import Link from 'next/link';
import HeaderPage from '../components/HeaderPage';
import { Typography, Alert } from 'antd';
import FooterPage from '../components/FooterPage';
import GlygenNote from '../components/GlygenNote';

const { Title, Text } = Typography;

interface Props {
  data: any
}

const routes = [
  {
    path: 'index',
    breadcrumbName: 'UniCarbKB-GlyGen',
  },
  {
    path: 'first',
    breadcrumbName: 'Curated',
  },
  {
    path: 'second',
    breadcrumbName: 'Glycoprotein',
  },
];


class Glycoproteins extends React.Component<Props> {


  state = {
    searchText: '',
    searchedColumn: '',
  };
  searchInput: Input;

  static async getInitialProps({req}) {

    const data = []
    const res = await fetch('https://storage.googleapis.com/unicarbkb_glycoproteins/allGlycoproteins.json');
    const json = await res.json();
  json.glycoproteins.glycoprotein.map( t => data.push( { key: t.accesion, accession: t.accession, name: t.fullName } )  );

    return { data }
  }

  getColumnSearchProps = dataIndex => ({
    filterDropdown: ({ setSelectedKeys, selectedKeys, confirm, clearFilters }) => (
      <div style={{ padding: 8 }}>
        <Input
          ref={node => {
            this.searchInput = node;
          }}
          placeholder={`Search ${dataIndex}`}
          value={selectedKeys[0]}
          onChange={e => setSelectedKeys(e.target.value ? [e.target.value] : [])}
          onPressEnter={() => this.handleSearch(selectedKeys, confirm, dataIndex)}
          style={{ width: 188, marginBottom: 8, display: 'block' }}
        />
        <Button
          type="primary"
          onClick={() => this.handleSearch(selectedKeys, confirm, dataIndex)}
          icon={<SearchOutlined />}
          size="small"
          style={{ width: 90, marginRight: 8 }}
        >
          Search
        </Button>
        <Button onClick={() => this.handleReset(clearFilters)} size="small" style={{ width: 90 }}>
          Reset
        </Button>
      </div>
    ),
    filterIcon: filtered => <SearchOutlined style={{ color: filtered ? '#1890ff' : undefined }} />,
    onFilter: (value, record) =>
      record[dataIndex]
        .toString()
        .toLowerCase()
        .includes(value.toLowerCase()),
    onFilterDropdownVisibleChange: visible => {
      if (visible) {
        setTimeout(() => this.searchInput.select());
      }
    },
    render: text =>
      this.state.searchedColumn === dataIndex ? (
        <Highlighter
          highlightStyle={{ backgroundColor: '#ffc069', padding: 0 }}
          searchWords={[this.state.searchText]}
          autoEscape
          textToHighlight={text.toString()}
        />
      ) : (
        text
      ),
  });

  handleSearch = (selectedKeys, confirm, dataIndex) => {
    confirm();
    this.setState({
      searchText: selectedKeys[0],
      searchedColumn: dataIndex,
    });
  };

  handleReset = clearFilters => {
    clearFilters();
    this.setState({ searchText: '' });
  };

  render() {


    const columns = [
      {
        title: 'Name',
        dataIndex: 'name',
        key: 'name',
        width: '30%',
        ...this.getColumnSearchProps('name')
      },
      {
        title: 'Accession',   
        dataIndex: 'accession',
        key: 'accession',
        width: '20%',
        //render: text => <Link href="www.bbc.com">{text}</Link>,
        ...this.getColumnSearchProps('accession'),
        render: accession =>  <Link href="/glycoprotein/" as={`/glycoprotein/${accession}`}><a>{accession}</a></Link>
      },
      
    ];

    return (
      <Layout className="layout">
       <HeaderPage />
       <Content style={{ padding: '0 50px', marginTop: 20 }}>
      <Breadcrumb style={{ margin: '24px 0' }}>
        <Breadcrumb.Item>Home</Breadcrumb.Item>
        <Breadcrumb.Item>Glycoproteins</Breadcrumb.Item>
      </Breadcrumb>
      <div style={{ background: '#fff', padding: 24 }}>
        <Title>Glycoproteins</Title>
        <GlygenNote />
        <Alert message="To filter the table click a magnifier glass and enter a UniProt accession or protein name of interest" type="info" /><br />
        <Table  rowKey="accession" columns={columns} dataSource={ this.props.data }  />
       
      </div>
    </Content>
    <FooterPage />
    
      </Layout>
    )
    
}
}




export default Glycoproteins ;