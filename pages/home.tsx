import React, { Component } from 'react';
import { Table, Row, Col } from 'antd';
import Layout from 'antd/lib/layout';
import Header from 'antd/lib/layout';
import Content from 'antd/lib/layout';
import DataTable from '../components/DataTable';
import fetch from 'isomorphic-unfetch';

interface IMyComponentState {
    airtablePosts: []
}

interface IMyComponentProps {
    someDefaultValue: string
}

class Home extends React.Component<IMyComponentProps, any>  {
    static getInitialProps: (context: any) => Promise<any>;
    constructor(props) {
      super(props)
  
      this.state = {
        airtablePosts: []
      }
    }
  
    componentDidMount() {
      const { props } = this
  
      const transferPosts = new Promise((resolve) => {
        const collectPosts = []
  
        Object.keys(props).map((item) => {
          // Filter out other props like 'url', etc.
          if (typeof props[item].id !== 'undefined') {
            collectPosts.push(props[item])
          }
        })
  
        resolve(collectPosts)
      })
  
      Promise.resolve(transferPosts).then(data => {
        this.setState({ airtablePosts: data })
      })
    }
  
    render() {

      const  x  = ({
        "uniprotId": 'Q13158',
alwaysLoadPredicted: true,
containerId: 'pluginContainer',
      })
      const { airtablePosts } = this.state
  
      if (!Array.isArray(airtablePosts) || !airtablePosts.length) {
        // Still loading Airtable data
        return (
          <Layout>
            <p>Loading&hellip;</p>
          </Layout>
        )
      }
      else {
        // Loaded
        return (
          <Layout>
            {airtablePosts.map((post) =>
              <p>{post.id}</p>
            )}
          </Layout>
        )
      }
    }
  }
  
  Home.getInitialProps = async (context) => {
    const basePath = (process.env.NODE_ENV === 'development') ? 'http://localhost:3000' : 'https://yourdomain.com'
  
    const res = await fetch(`${basePath}/api/get/posts`)
    const airtablePosts = await res.json()
  
    return airtablePosts ? airtablePosts.data : {}
  }
  
  export default Home