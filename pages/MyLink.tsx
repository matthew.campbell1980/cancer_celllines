import Link from 'next/link'
import React from 'react'
import Select from 'react-select';

const options = [
    { value: 'chocolate', label: 'Chocolate' },
    { value: 'strawberry', label: 'Strawberry' },
    { value: 'vanilla', label: 'Vanilla' },
  ];
const demos = {
    soundcloud:
      '<iframe width="90% seamless="seamless"" height="525" frameborder="0" scrolling="no" src="//plotly.com/~mpcamp/1.embed"></iframe>',
    
      site17:
      '<iframe width="auto" height="auto" frameborder="0" scrolling="no" src="//plotly.com/~mpcamp/4.embed"></iframe>',
  
      site61:
      '<iframe width="900" height="800" frameborder="0" scrolling="no" src="//plotly.com/~mpcamp/11.embed"></iframe>',
      site74:
      '<iframe width="900" height="800" frameborder="0" scrolling="no" src="//plotly.com/~mpcamp/13.embed"></iframe>',
  
      site122:
      '<iframe width="900" height="800" frameborder="0" scrolling="no" src="//plotly.com/~mpcamp/18.embed"></iframe>',
  
      site149:
      '<iframe width="900" height="800" frameborder="0" scrolling="no" src="//plotly.com/~mpcamp/20.embed"></iframe>',
  
      site165:
      '<iframe width="900" height="800" frameborder="0" scrolling="no" src="//plotly.com/~mpcamp/22.embed"></iframe>',
  
      site234:
      '<iframe width="900" height="800" frameborder="0" scrolling="no" src="//plotly.com/~mpcamp/24.embed"></iframe>',
  
      site282:
      '<iframe width="900" height="800" frameborder="0" scrolling="no" src="//plotly.com/~mpcamp/26.embed"></iframe>',
  
      site331:
      '<iframe width="900" height="800" frameborder="0" scrolling="no" src="//plotly.com/~mpcamp/28.embed"></iframe>',
  
      site343:
      '<iframe width="900" height="800" frameborder="0" scrolling="no" src="//plotly.com/~mpcamp/30.embed"></iframe>',
  
      site603:
      '<iframe width="900" height="800" frameborder="0" scrolling="no" src="//plotly.com/~mpcamp/32.embed"></iframe>',
  
      site616:
      '<iframe width="900" height="800" frameborder="0" scrolling="no" src="//plotly.com/~mpcamp/34.embed"></iframe>',
  
      site657:
      '<iframe width="900" height="800" frameborder="0" scrolling="no" src="//plotly.com/~mpcamp/36.embed"></iframe>',
  
      site709:
      '<iframe width="900" height="800" frameborder="0" scrolling="no" src="//plotly.com/~mpcamp/38.embed"></iframe>',
  
      site717:
      '<iframe width="900" height="800" frameborder="0" scrolling="no" src="//plotly.com/~mpcamp/40.embed"></iframe>',
  
      site801:
      '<iframe width="900" height="800" frameborder="0" scrolling="no" src="//plotly.com/~mpcamp/42.embed"></iframe>',
  
      site1074:
      '<iframe width="900" height="800" frameborder="0" scrolling="no" src="//plotly.com/~mpcamp/44.embed"></iframe>',
  
      site1098:
      '<iframe width="900" height="800" frameborder="0" scrolling="no" src="//plotly.com/~mpcamp/46.embed"></iframe>',
  
      site1134:
      '<iframe width="900" height="800" frameborder="0" scrolling="no" src="//plotly.com/~mpcamp/48.embed"></iframe>',
  
      site1158:
      '<iframe width="900" height="800" frameborder="0" scrolling="no" src="//plotly.com/~mpcamp/50.embed"></iframe>',
  
      site1173:
      '<iframe width="900" height="800" frameborder="0" scrolling="no" src="//plotly.com/~mpcamp/52.embed"></iframe>',
  
      site1194:
      '<iframe width="900" height="800" frameborder="0" scrolling="no" src="//plotly.com/~mpcamp/54.embed"></iframe>',
  };

  function Iframe(props) {
    return (
      <div
        dangerouslySetInnerHTML={{ __html: props.iframe ? props.iframe : "" }}
      />
    );
  }

class MyLink extends React.Component {
    // This syntax ensures `this` is bound within handleClick.
    // Warning: this is *experimental* syntax.
    handleClick = () => {
      console.log('this is:', this);
    }

    state = {
        isActive: false
      };
    
      handleShow = () => {
        this.setState({isActive: true});
      };
    
      handleHide = () => {
        this.setState({isActive: false});
      };


 
    render() {

      

    
           
        if (this.state.isActive) {
            return (
              <div>
                <h1>Hello react</h1>
                <div>
          <Iframe iframe={demos["site17"]} />,
        </div>
                <button onClick={this.handleHide}>Hide</button>
              </div>
              
              
            );
          } else {
            return (
              <div>
                <button onClick={this.handleShow}>Show</button>
              </div>
            );
          }
      
    }
  }

  export default MyLink