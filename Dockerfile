
FROM node:10
#16-alpine
WORKDIR /app
COPY package.json /app/
ENV PATH /app/node_modules/.bin:$PATH

#RUN npm install -g npm@7.22.0
RUN npm install 
#next react react-dom    



#COPY yarn.lock /app/
#RUN apk add --no-cache git
#RUN yarn install
#COPY package-lock.json /app/
#RUN npm install -g npm@7.22.0
#RUN npm i

COPY . /app
#RUN yarn build
RUN npm run start
# start app
#CMD [ "yarn", "start" ]
CMD [ "npm", "start" ]